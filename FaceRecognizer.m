//
//  FaceRecognizer.m
//  Face_Teacher
//
//  Created by jackstevenson on 2018/4/27.
//  Copyright © 2018年 jackstevenson. All rights reserved.
//

#import "FaceRecognizer.h"

@interface FaceRecognizer()
@property (nonatomic, strong) ToolsForFaceRecognizer *tools;
@property (nonatomic, strong) FRmodel *model;
@end

@implementation FaceRecognizer

const double threshold = 0.7;

-(ToolsForFaceRecognizer *)tools
{
    if(!_tools) _tools = [[ToolsForFaceRecognizer alloc]init];
    return _tools;
}

-(FRmodel *)model
{
    if(!_model) _model = [[FRmodel alloc]init];
    return _model;
}

-(double)L2DistanceBetween:(MLMultiArray *)origin andSample:(MLMultiArray *)sample
{
    //NSLog(@"L2_dist_betwwen:%@, and\n %@", origin, sample);
    long vector_len = origin.count;
    double L2_dist = 0;
    for(long i = 0; i < vector_len; i++)
    {
        L2_dist += (origin[i].doubleValue - sample[i].doubleValue) * (origin[i].doubleValue - sample[i].doubleValue);
    }
    L2_dist = sqrt(L2_dist);
    return L2_dist;
}

-(BOOL)isSamePerson:(MLMultiArray *)originalFace_vector and:(MLMultiArray *)sampleFace_vector
{
    double L2_dist = [self L2DistanceBetween:originalFace_vector andSample:sampleFace_vector];
    NSLog(@"L2_dist:%lf", L2_dist);
    return L2_dist < threshold;
}

-(MLMultiArray *)imageToVectorOfMLArray:(UIImage *)faceImage
{
    NSError *error;
    FRmodelOutput *result;
    UIImage *resizedFaceImage = [self.tools imageWithImage:faceImage scaledToSize:CGSizeMake(96 / 2, 96 / 2)];
    //[OpencvTools reszie:faceImage To:CGSizeMake(96, 96)];
    @try
    {
        result = [self.model predictionFromFace_image:[self.tools imageToArray:resizedFaceImage] error:&error];
        if(error)
        {
            NSLog(@"Error in imageToVector:%@", error.description);
            return nil;
        }
        //NSLog(@"image to vector:%@", [self.tools L2_Norm:result.output]);
        return [self.tools L2_Norm:result.output];
        
    }
    @catch(NSException *exception)
    {
        NSLog(@"Error %@", exception.description);
        return nil;
    }
}

-(NSArray *)mlArrayToNSArray:(MLMultiArray *)mlarray
{
    NSMutableArray *array = [[NSMutableArray alloc]init];
    unsigned long i, len = mlarray.count;
    for(i = 0; i < len; i++)
    {
        [array addObject:mlarray[i]];
    }
    return [[NSArray alloc]initWithArray:array];
}
-(NSArray *)imageToVectorOfNSArray:(UIImage *)faceImage
{
    MLMultiArray *MLVector = [self imageToVectorOfMLArray:faceImage];
    return [self mlArrayToNSArray:MLVector];
}

-(NSString *)vectorToJsonStr:(MLMultiArray *)faceVector
{
    NSArray *nsvector = [self mlArrayToNSArray:faceVector];
    NSLog(@"vector in convertor:%@", nsvector);
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:nsvector options:NSJSONWritingPrettyPrinted error:&error];
    if(error) NSLog(@"error in converting nsvector to json data:%@", error.description);
    return [[NSString alloc]initWithData:jsonData encoding:NSUTF8StringEncoding];
}



-(NSString *)findFaceImage:(UIImage *)faceImage InDataSet:(NSDictionary *)dataSet
{
    faceImage = [self.tools imageWithImage:faceImage scaledToSize:CGSizeMake(96 / 2, 96 / 2)];
    MLMultiArray *facevector = [self imageToVectorOfMLArray:faceImage];
    NSString *name;
    double L2_dist = 1000000, dist;
    NSArray *keys = [dataSet allKeys];
    for(int i = 0; i < keys.count; i++)
    {
        NSDictionary *info = dataSet[keys[i]];
        MLMultiArray *vector = info[@"vector"];
        dist = [self L2DistanceBetween:facevector andSample:vector];
        if(dist < L2_dist)
        {
            L2_dist = dist;
            name = [NSString stringWithString:(NSString *)info[@"name"]];
        }
    }
    NSLog(@"L2_dist:%lf", L2_dist);
    if(L2_dist < threshold) return name;
    else return nil;
}
@end
