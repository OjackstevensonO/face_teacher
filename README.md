# README #

* 该代码库仅用做求职申请的补充材料，不用做其他用途

### 本app用途 ###

* 教学环境下，基于简单的面部识别技术实现学生出勤统计
* (全离线方案已完成模型移植等工作)

### 使用方法 ###

* 将移动设备放置于教室门口，持续对视频流中出现的人脸进行检测和识别，检出本课堂同学的人脸后记录出勤信息

### 在线方案实现原理 ###
* 关键词：OpenCV 人脸检测 目标跟踪 人脸识别 
* 设计原理：通过iOS设备摄像头获取实时的视频流，在本地通过人脸检测算法定位出人脸，通过目标跟踪算法跟踪已定位的人脸，将第一次出现在视频监控区域的人脸裁剪压缩后上传至服务器进行人脸识别，并将结果返回。
* 为什么需要目标跟踪：远程服务器仅接受静态图像的人脸识别，而视频流中出现的是动态人脸，这意味着只要有人进入视频区域且尚未离开，则人脸检测算法在视频流的每一帧中均会检测出人脸(大多数时候是同一张人脸)。那么每秒就会产生几十张人脸照片，而这些照片大多是重复的，若将其全部提交则意味着每秒会有几十个HTTP请求产生，而这对于远程服务器和本地设备都是不可接受的。远程服务器的公有接口一般情况下不支持这样多的并发数。所以需要目标跟踪算法进行重复样本（粗）过滤。
### 相关模型 ###
* 在线识别方案时序图
![Alt text](https://0mlhcq.bn.files.1drv.com/y4mac7EBu-7Rin7Ok9-GLpVuQPH4gISHqMwaQDIvBAHfc-Z2IXvWBsUtZuo0OZIFGUhOZEvTtB4v2KIT4lZJBAvXzI8OhMvx0Lq-PtzZH9TGdqyg8Hp5RiPw_VJcusPmqGLamvCBcfi8dvqiKLSea3YAqW8R-fhy2AXf-neGUN_y0pjs7cIBFtOCA7jc2bHzYLH-f0OJvjRtftv93ewl4GeTA/sequence_model.png?psid=1)
  图1 在线识别方案时序图
* 软件行为模型
![Alt text](https://22jj5g.bn.files.1drv.com/y4md7hJ6XAinnAgEjAJw2J5rP79kuqJT5QDL0Ewknk13C9iC7WkRe8rRADrztIaknxtMiHePsZDzyyeIRwzw-U0IdCN_pA8KBFLkqsECurJC5difaPhkWK2uvvDpYMW4ygvABt6Ez_0J-VtNpHEA6gFOJxGh-o1Pb-GfMXnRvd3rpGvqSYBrhFTAMKmjgiXCuBDNjjbYilM9fnUxHWV_Yhtyw/state_model.png?psid=1)
  图2 软件行为模型
* 相关类图
见PDF文件(无需登录即可查看,若提示需要登录,关闭登录小窗口即可)-->[链接](https://1drv.ms/b/s!AvaHtIsH6vnKgYQY1xFo4tMy4gKPTA)
### 离线方案原理 ###
* 使用FaceNet作为人脸识别方案。FaceNet作用为将人脸转换为一个128维的向量，通过计算两张人脸输出的向量的欧式距离来判断两张人脸的相似程度。若欧式距离小于预设的阈值，则认为是用一个人