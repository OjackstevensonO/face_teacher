//
//  MultiFaceTracker.h
//  Face_Teacher
//
//  Created by jackstevenson on 17/5/12.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreImage/CoreImage.h>
#import <UIKit/UIKit.h>

@interface MultiFaceTracker : NSObject
-(BOOL)isTracking;//是否有目标正在被跟踪
-(void)addTrackingObject:(CGRect)objectRect InFrame:(UIImage *)image;
-(void)updateFrame:(UIImage *)image;
-(BOOL)isNewFace:(CGRect)faceRect;
-(NSArray *)trackingObejctRects;
@end
