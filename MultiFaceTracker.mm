//
//  MultiFaceTracker.m
//  Face_Teacher
//
//  Created by jackstevenson on 17/5/12.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//
#import <opencv2/opencv.hpp>
#import <opencv2/tracking.hpp>
#import <opencv2/imgproc.hpp>
#import <opencv2/imgcodecs/ios.h>
#import "FaceTracker.h"
#import "MultiFaceTracker.h"


@interface MultiFaceTracker()
@property (strong, nonatomic) NSMutableArray *trackers;
@end

@implementation MultiFaceTracker

-(BOOL)isNewFace:(CGRect)faceRect
{
    if(![self.trackers count]) return YES;
    for(FaceTracker *tracker in self.trackers)
    {
        if(![tracker isNewFace:faceRect]) return NO;
    }
    return YES;
}

-(NSMutableArray *)trackers
{
    if(!_trackers) _trackers = [[NSMutableArray alloc]init];
    return _trackers;
}

-(BOOL)isTracking
{
    return [self.trackers count];
}

-(void)addTrackingObject:(CGRect)objectRect InFrame:(UIImage *)image
{
    FaceTracker *tracker = [[FaceTracker alloc]init];
    [tracker startWithFrame:image andBox:objectRect];
    [self.trackers addObject:tracker];
}

-(void)updateFrame:(UIImage *)image
{
    for(FaceTracker *tracker in self.trackers)
    {
        [tracker updateFrame:image];
    }
    NSMutableArray *deletObejcts = [[NSMutableArray alloc]init];
    for(FaceTracker *tracker in self.trackers)
    {
        if([tracker objectIsOutOfbound])
        {
            [deletObejcts addObject:tracker];
            //[self.trackers removeObject:tracker];
        }
    }
    for(FaceTracker *tracker in deletObejcts)
    {
        [self.trackers removeObject:tracker];
    }
}

-(NSArray *)trackingObejctRects
{
    NSMutableArray *tempRects = [[NSMutableArray alloc]init];
    for(FaceTracker *tracker in self.trackers)
    {
        CGRect rect = [tracker getTrackingRectBox];
        [tempRects addObject:[NSValue valueWithCGRect:rect]];
    }
    return [[NSArray alloc]initWithArray:tempRects];
}

@end
