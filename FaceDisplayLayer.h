//
//  FaceDisplayLayer.h
//  Face_Teacher
//
//  Created by jackstevenson on 17/4/19.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface FaceDisplayLayer : CALayer
@property (strong, nonatomic) NSArray *faces;
@property (nonatomic) CGRect trackerBox;
-(void)setWithParentLayer:(UIView *)parentView;
-(instancetype)init;
@end
