//
//  ToolsForFaceRecognizer.h
//  Face_Teacher
//
//  Created by jackstevenson on 2018/4/27.
//  Copyright © 2018年 jackstevenson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreML/CoreML.h>
#import <CoreImage/CoreImage.h>

@interface ToolsForFaceRecognizer : NSObject
-(double)around:(double)num decimals:(int)len;
-(MLMultiArray *)L2_Norm:(MLMultiArray *)vector;
-(MLMultiArray *)imageToArray:(UIImage *)image;
- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
@end
