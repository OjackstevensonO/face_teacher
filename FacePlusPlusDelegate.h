//
//  FacePlusPlusDelegate.h
//  Face_Teacher
//
//  Created by jackstevenson on 17/4/28.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol FacePlusPlusDelegate <NSObject>
@optional
-(void)finishFaceRecognition:(NSData *)data;
-(void)finishAddFaceToServer:(NSData *)data usingFaceToken:(NSString *)face_token;
-(void)dealWithError:(NSData *)data response:(NSURLResponse *)response andError:(NSError *)error;
-(void)removeFaceSuccess:(NSData *)data;
@end
