//
//  UIImage+UIImageCut.h
//  Face_Teacher
//
//  Created by jackstevenson on 17/4/28.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UIImageCut)
-(UIImage*)getSubImage:(CGRect)rect;
-(UIImage*)scaleToSize:(CGSize)size;
-(UIImage *)cutImageatRect:(CGRect)rect;
@end
