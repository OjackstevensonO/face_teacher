//
//  OpencvTools.m
//  Face_Teacher
//
//  Created by jackstevenson on 17/4/28.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import <opencv2/opencv.hpp>
#import <opencv2/core.hpp>
#import <opencv2/imgproc.hpp>
#import <opencv2/imgcodecs/ios.h>
#import "OpencvTools.h"

@implementation OpencvTools
+(UIImage *)reszie:(UIImage *)image To:(CGSize)size
{
    UIImage *newImage;
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);
    cv::resize(imageMat, imageMat, cv::Size((int)size.width, (int)size.height));
    newImage = MatToUIImage(imageMat);
    return newImage;
}
+(UIImage *)getPGMImageFromPath:(NSString *)path
{
    cv::Mat image;
    const CFIndex LEN = 2048;
    char *path_c = (char *) malloc(LEN);
    CFStringGetFileSystemRepresentation( (CFStringRef)path, path_c, LEN);
    image = cv::imread(path_c);
    free(path_c);
    return MatToUIImage(image);
}

@end
