//
//  GlobalSetting.h
//  Face_Teacher
//
//  Created by jackstevenson on 17/3/18.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
//全局参数定义
#define TEACHINGCLASS_ENTITYNAME @"TeachingClass"
#define TEACHING_CLASS_NAME @"name"
#define STUDENT_ENTITYNAME @"Student"
#define STUDENT_PROPERTY_ID @"idNumber"
#define RECORD_TABLE_ENTITYNAME @"RecordTable"
#define RECORD_ENTITYNAME @"Record"
#define FACE_RECOGNITION_SCHEME @"face_recognition_scheme"
#define OPENCV 0
#define CIDETECTOR 1
static NSString *faceplusplus_api_create_faceset_url = @"https://api-cn.faceplusplus.com/facepp/v3/faceset/create";
static NSString *faceplusplus_api_key = @"1Fs2xKWRVCT7AoJb6EG_xvjIUTBumsUF";
static NSString *faceplusplus_api_secret = @"bCVSgiOkPzoiZ2gvmawMf3EY_whh0-oN";
static NSString *faceplusplus_api_faceset_token = @"7afe70898d18122eafe130e947716887";
static NSString *faceplusplus_api_faceset_addface_url = @"https://api-cn.faceplusplus.com/facepp/v3/faceset/addface";
static NSString *faceplusplus_api_faceset_detect_url = @"https://api-cn.faceplusplus.com/facepp/v3/detect";
static NSString *faceplusplus_api_search_face_url = @"https://api-cn.faceplusplus.com/facepp/v3/search";
static NSString *faceplusplus_api_faceset_removeface = @"https://api-cn.faceplusplus.com/facepp/v3/faceset/removeface";
static double face_recognition_confidence_threshold = 80.0;

typedef enum : NSUInteger {
    USING_OPENCV,
    USING_CIDETECTOR
}FaceDetectorTool;

typedef enum : NSUInteger{
    USING_FACE_PLUS_PLUS,
    USING_FACENET
}FaceRecognitionTool;
//全局功能性定义
#define USING_SCLALERTVIEW_TO_REPLACE_SYSTEMALERTVIEW
#define CURRENT_FACE_DETECTOR CIDETECTOR

//ERROR CODE
#define UNIQUEKEY_CONFLICT 77309001
@interface GlobalSetting : NSObject
@end
