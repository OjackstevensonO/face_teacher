//
//  EditStudentViewController.h
//  Face_Teacher
//
//  Created by jackstevenson on 17/5/30.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Student+CoreDataClass.h"
#import "StudentsOfClassCollectionViewController.h"

@interface EditStudentViewController : UIViewController
@property (strong, nonatomic) Student *student;
@property (weak, nonatomic) StudentsOfClassCollectionViewController *delegate;
@end
