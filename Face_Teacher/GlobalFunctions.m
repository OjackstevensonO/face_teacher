//
//  GlobalFunctions.m
//  Face_Teacher
//
//  Created by jackstevenson on 17/3/18.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import "GlobalFunctions.h"
#import "AppDelegate.h"
@implementation GlobalFunctions

+(NSManagedObjectContext *)getContext
{
    AppDelegate *deleget = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSPersistentContainer *container = [deleget persistentContainer];
    return container.viewContext;
}

+(BOOL)value:(NSString *)value ofKey:(NSString *)keyName isExistInEntity:(NSString *)entityName Error:(NSError **)error
{
    NSManagedObjectContext *context = [GlobalFunctions getContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entityName];
    request.predicate = [NSPredicate predicateWithFormat:@"%K == %@", keyName, value];
    NSArray *isExist = [context executeFetchRequest:request error:error];
    if([isExist count])
    {
        *error = [NSError errorWithDomain:@"unique key conflict"
                                           code:UNIQUEKEY_CONFLICT
                                       userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Value(%@) For KEY(%@) 已存在", value, keyName],
                                                  NSLocalizedFailureReasonErrorKey:[NSString stringWithFormat:@"%d", UNIQUEKEY_CONFLICT]}];
        return YES;
    }
    return NO;
}


+(void)showErrorAlertTo:(UIViewController *)controller withTitle:(NSString *)title Message:(NSString *)message buttonName:(NSString *)buttonName
{
    NSString *kSuccessTitle = title;
    NSString *kButtonTitle = buttonName;
    NSString *kSubtitle = message;
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    alert.soundURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/right_answer.mp3", [NSBundle mainBundle].resourcePath]];
    [alert showError:kSuccessTitle subTitle:kSubtitle closeButtonTitle:kButtonTitle duration:0.0f];
}

+(void)showErrorAlertTo:(UIViewController *)controller withTitle:(NSString *)title Message:(NSString *)message buttonName:(NSString *)buttonName confirm:(SCLActionBlock)confirmActionBlock withCancelButton:(NSString *)cancelButtonName canceled:(SCLActionBlock)cancelBlock
{
    NSString *kSuccessTitle = title;
    NSString *kButtonTitle = buttonName;
    NSString *kSubtitle = message;
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    alert.soundURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/right_answer.mp3", [NSBundle mainBundle].resourcePath]];
    [alert addButton:kButtonTitle actionBlock:confirmActionBlock];
    [alert addButton:cancelButtonName actionBlock:cancelBlock];
    [alert showError:kSuccessTitle subTitle:kSubtitle closeButtonTitle:nil duration:0.0f];
}


+(void)showSuccessAlertTo:(UIViewController *)controller withTitle:(NSString *)title Message:(NSString *)message buttonName:(NSString *)buttonName
{
    NSString *kSuccessTitle = title;
    NSString *kButtonTitle = buttonName;
    NSString *kSubtitle = message;
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    alert.soundURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/right_answer.mp3", [NSBundle mainBundle].resourcePath]];
    [alert showSuccess:kSuccessTitle subTitle:kSubtitle closeButtonTitle:kButtonTitle duration:0.0f];
}

+(void)showSuccessAlertTo:(UIViewController *)controller withTitle:(NSString *)title Message:(NSString *)message buttonName:(NSString *)buttonName duration:(NSTimeInterval)time
{
    NSString *kSuccessTitle = title;
    NSString *kButtonTitle = buttonName;
    NSString *kSubtitle = message;
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    alert.soundURL = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/right_answer.mp3", [NSBundle mainBundle].resourcePath]];
    [alert showSuccess:kSuccessTitle subTitle:kSubtitle closeButtonTitle:kButtonTitle duration:time];
}

+(void)deletStudentsImagesIn:(TeachingClass *)teachingClass
{
    for(Student *student in teachingClass.students)
    {
        // Create path.
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:student.idNumber];
        [[NSFileManager defaultManager] removeItemAtPath:filePath error:nil];
    }
}

+(BOOL)isIntersect:(CGRect)rectA andRectB:(CGRect)rectB
{
    if(rectA.origin.x > rectB.origin.x)
    {
        CGRect temp = rectA;
        rectA = rectB;
        rectB = temp;
    }
    if(rectA.origin.x + rectA.size.width >= rectB.origin.x)
    {
        if(rectA.origin.y <= rectB.origin.y + rectB.size.height && rectB.origin.y + rectB.size.height <= rectA.origin.y + rectA.size.height) return YES;
        if(rectA.origin.y <= rectB.origin.y && rectA.origin.y + rectA.size.height >= rectB.origin.y) return YES;
        if(rectA.origin.y >= rectB.origin.y && rectA.origin.y + rectA.size.height <= rectB.origin.y + rectB.size.height) return YES;
    }
    return NO;
}

+(NSString *)removeSpareCharIn:(NSString *)string
{
    NSString *t = [NSString stringWithString:string];
    t = [t stringByReplacingOccurrencesOfString:@" " withString:@""];
    t = [t stringByReplacingOccurrencesOfString:@"(" withString:@""];
    t = [t stringByReplacingOccurrencesOfString:@")" withString:@""];
    t = [t stringByReplacingOccurrencesOfString:@"\r" withString:@""];
    t = [t stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    return t;
}

+(NSDictionary *)getNameOfFacetokenMapping
{
    NSDictionary *dic;
    NSManagedObjectContext *context = [self getContext];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:STUDENT_ENTITYNAME];
    NSArray *studentArray = [context executeFetchRequest:request error:nil];
    NSMutableDictionary *tempArray = [[NSMutableDictionary alloc]init];
    for(Student *s in studentArray)
    {
        if(s.face_token) [tempArray setValue:s.name forKey:s.face_token];
    }
    dic = [NSDictionary dictionaryWithDictionary:tempArray];
    return dic;
}

@end
