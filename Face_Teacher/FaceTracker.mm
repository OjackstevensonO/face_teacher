//
//  FaceTracker.m
//  Face_Teacher
//
//  Created by jackstevenson on 17/4/23.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//



#import "FaceTracker.h"
#import "CompressiveTracker.h"
#import "GlobalFunctions.h"

static NSInteger clipWidth = 10;

@interface FaceTracker()
{
    cv::Rect2d _roi;
    cv::Ptr<cv::Tracker> _cvTracker;
    CompressiveTracker ct;
    cmt::CMT *_cmtTracker;
    //cv::Rect _roi;
}
@property (nonatomic) BOOL trackerWasInited;//私有变量
@property (nonatomic) BOOL isTracking;
@property (nonatomic) CGRect bound;
@end

@implementation FaceTracker

-(BOOL)isNewFace:(CGRect)faceRect
{
    if([GlobalFunctions isIntersect:faceRect andRectB:[self getTrackingRectBox]]) return NO;
    return YES;
}



//KCF Tracking


- (CGRect)getTrackingRectBox
{
    CGRect rect = CGRectMake(_roi.tl().x, _roi.tl().y, _roi.size().width, _roi.size().height);
    //NSLog(@"%f %f", rect.origin.x, rect.origin.y);
    //NSLog(@"%d", CGRectContainsRect(self.bound, rect));
    return rect;
}

-(void)updateFrame:(UIImage *)frameImage
{
    cv::Mat frame;
    UIImageToMat(frameImage, frame, YES);
    cv::cvtColor(frame, frame, CV_BGRA2BGR);
    _cvTracker->update(frame, _roi);
}

-(void)startWithFrame:(UIImage *)frameImage andBox:(CGRect)objectBox
{
    _cvTracker = cv::Tracker::create("KCF");
    cv::Mat frame;
    UIImageToMat(frameImage, frame, YES);
    cv:cvtColor(frame, frame, CV_BGRA2BGR);
    _roi = cv::Rect2d(objectBox.origin.x, objectBox.origin.y, objectBox.size.width, objectBox.size.height);
    _cvTracker->clear();
    _cvTracker->init(frame, _roi);
    self.bound = CGRectMake(clipWidth, clipWidth, frameImage.size.width - clipWidth, frameImage.size.height - clipWidth);
    NSLog(@"Tracker init");
}

- (void)stopTracker
{
    if(_cvTracker) delete _cvTracker;
    _roi = cv::Rect2d(0, 0, 0, 0);
}

- (BOOL)trackerIsTracking
{
    return _cvTracker != NULL;
}

-(BOOL)objectIsOutOfbound
{
    return !CGRectContainsRect(self.bound, [self getTrackingRectBox]);
}

//CMT tracking
//-(void)releaseTracker
//{
//    if (_cmtTracker != NULL)
//    {
//        delete _cmtTracker;
//        _roi = cv::Rect(0, 0, 0, 0);
//    }
//}
//
//- (CGRect)getTrackingRectBox
//{
//    CGRect rect = CGRectMake(_roi.tl().x, _roi.tl().y, _roi.size().width, _roi.size().height);
//    return rect;
//}
//
//-(void)updateFrame:(UIImage *)frameImage andDrawResultIn:(cv::Mat &)imageMat
//{
//    cv::Mat grayFrame;
//    UIImageToMat(frameImage, grayFrame);
//    cv::cvtColor(grayFrame, grayFrame, CV_RGB2GRAY);
//    _cmtTracker->processFrame(grayFrame);
//    for(size_t i = 0; i < _cmtTracker->points_active.size(); i++)
//    {
//        circle(imageMat, _cmtTracker->points_active[i], 2, cv::Scalar(255,0,0));
//    }
//    RotatedRect rect = _cmtTracker->bb_rot;
//    Point2f vertices[4];
//    rect.points(vertices);
//    for (int i = 0; i < 4; i++)
//    {
//        line(imageMat, vertices[i], vertices[(i+1)%4], cv::Scalar(255,0,255));
//    }
//}
//
//-(instancetype)initFrame:(UIImage *)frameImage andBox:(CGRect)objectBox
//{
//    self = [super init];
//    if(self)
//    {
//        cv::Mat grayFrame;
//        UIImageToMat(frameImage, grayFrame);
//        cv::cvtColor(grayFrame, grayFrame, CV_RGB2GRAY);
//        _roi = cv::Rect(objectBox.origin.x, objectBox.origin.y, objectBox.size.width, objectBox.size.height);
//        _cmtTracker = new cmt::CMT();
//        cv::Rect temp_roi = cv::Rect(_roi);
//        temp_roi.x -= temp_roi.width * (1 - scalarFactor) / 2;
//        temp_roi.y -= temp_roi.height * (1 - scalarFactor) / 2;
//        temp_roi.width *= scalarFactor;
//        temp_roi.height *= scalarFactor;
//        _cmtTracker->initialize(grayFrame, temp_roi);
//        //NSLog(@"cmt track init!");
//        self.isTracking = true;
//    }
//    return self;
//}


//CT Tracking

//- (CGRect)getTrackingRectBox
//{
//    CGRect rect = CGRectMake(_roi.tl().x, _roi.tl().y, _roi.size().width, _roi.size().height);
//    return rect;
//}
//
//-(void)updateFrame:(UIImage *)frameImage
//{
//    cv::Mat frame;
//    UIImageToMat(frameImage, frame, YES);
//    cv::cvtColor(frame, frame, CV_RGBA2RGB);
//    ct.processFrame(frame, _roi);
//}
//
//-(instancetype)initFrame:(UIImage *)frameImage andBox:(CGRect)objectBox
//{
//    self = [super init];
//    if(self)
//    {
//        cv::Mat frame;
//        UIImageToMat(frameImage, frame, YES);
//        cv::cvtColor(frame, frame, CV_RGBA2RGB);
//        _roi = cv::Rect(objectBox.origin.x, objectBox.origin.y, objectBox.size.width, objectBox.size.height);
//        ct.init(frame, _roi);
//    }
//    return self;
//}


//CT 算法demo
/*
-(void)initFrame:(cv::Mat)frame andBox:(cv::Rect)box
{
    cv::Mat grayImg;
    cvtColor(frame, grayImg, CV_RGBA2GRAY);
    self.ct.init(grayImg, box);
    self.trackingBox = CGRectMake(box.x, box.y, box.size().width, box.size().height);
    self.trackerWasInited = YES;
}

-(void)setNextFrame:(cv::Mat)frame
{
    cv::Mat grayImg;
    cv::Rect box;
    cvtColor(frame, grayImg, CV_RGBA2GRAY);
    self.ct.processFrame(grayImg, box);// Process frame
    CGRect boxRect = CGRectMake(box.x, box.y, box.size().width, box.size().height);
    self.trackingBox = boxRect;
}

-(CGRect)getTrackingRectBox
{
    if(self.trackerWasInited) return self.trackingBox;
    else return CGRectMake(0, 0, 0, 0);
}
*/

@end
