//
//  StudentCollectionViewCell.h
//  Face_Teacher
//
//  Created by jackstevenson on 17/3/28.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Student+CoreDataClass.h"
@interface StudentCollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *studentPhoto;
@property (weak, nonatomic) IBOutlet UILabel *studentNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *studentIDNumberLabel;
@property (nonatomic, strong) Student *student;
@end
