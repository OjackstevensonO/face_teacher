//
//  StudentCollectionViewCell.m
//  Face_Teacher
//
//  Created by jackstevenson on 17/3/28.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import "StudentCollectionViewCell.h"

@implementation StudentCollectionViewCell
- (void)setStudent:(Student *)student
{
    _student = student;
    self.studentNameLabel.text = _student.name;
    self.studentIDNumberLabel.text = _student.idNumber;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:student.idNumber];
    self.studentPhoto.image = [UIImage imageWithContentsOfFile:filePath];
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
//        self.studentPhoto.image = [UIImage imageWithContentsOfFile:filePath];
//    });
}
@end
