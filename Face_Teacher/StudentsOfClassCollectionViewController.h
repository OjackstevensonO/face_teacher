//
//  StudentsOfClassCollectionViewController.h
//  Face_Teacher
//
//  Created by jackstevenson on 17/3/28.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "GlobalSetting.h"
@interface StudentsOfClassCollectionViewController : UIViewController
@property (nonatomic, strong) NSString *className;
-(void)reloadCollectionView;
@end
