//
//  AttendanceTableViewController.m
//  Face_Teacher
//
//  Created by jackstevenson on 17/6/10.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import "AttendanceTableViewController.h"
#import "GlobalSetting.h"
#import "AppDelegate.h"
#import "Student+CoreDataClass.h"

@interface AttendanceTableViewController ()
@property (nonatomic, strong) NSArray *attendanceSec;
@property (nonatomic, strong) NSArray *absenceSec;
@property (nonatomic, strong) NSManagedObjectContext *context;
@end

@implementation AttendanceTableViewController

- (NSManagedObjectContext *)context
{
    if(!_context)
    {
        AppDelegate *_appDeleget = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSPersistentContainer *persistentContainer = [_appDeleget persistentContainer];
        _context =  persistentContainer.viewContext;
    }
    return _context;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.attendanceSec = [self.record.attendanceStudents allObjects];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:STUDENT_ENTITYNAME];
    NSArray *allStu = [self.context executeFetchRequest:request error:nil];
    NSMutableArray *sec2 = [[NSMutableArray alloc]init];
    for(Student *s in allStu)
    {
        if(![self.record.attendanceStudents containsObject:s])
        {
            [sec2 addObject:s];
        }
    }
    self.absenceSec = [NSArray arrayWithArray:sec2];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    if(section == 0) return self.attendanceSec.count;
    else return self.absenceSec.count;
}

//- (NSArray<NSString *> *)sectionIndexTitlesForTableView:(UITableView *)tableView
//{
//    NSArray *sectionTitle = @[@"出席人员名单", @"缺席人员名单"];
//    return sectionTitle;
//}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSArray *sectionTitle = @[@"出席人员名单", @"缺席人员名单"];
    return sectionTitle[section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"studentNameCell" forIndexPath:indexPath];
    
    // Configure the cell...
    NSString *name;
    if(indexPath.section == 0)
    {
        name = ((Student *)self.attendanceSec[indexPath.row]).name;
    }
    else
    {
        name = ((Student *)self.absenceSec[indexPath.row]).name;
    }
    cell.textLabel.text = name;
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
