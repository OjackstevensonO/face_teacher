//
//  ClassManagerTableViewController.m
//  FaceTeachingRoom_Teacher
//
//  Created by jackstevenson on 2017/3/10.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import "ClassManagerTableViewController.h"
#import "StudentsOfClassCollectionViewController.h"
@interface ClassManagerTableViewController ()
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *context;
@end

@implementation ClassManagerTableViewController

- (NSManagedObjectContext *)context
{
    if(!_context)
    {
        AppDelegate *appDeleget = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSPersistentContainer *container = [appDeleget persistentContainer];
        _context = [container viewContext];
    }
    return _context;
}

-(NSArray *)addMenuArray
{
    return @[@"创建班级",@"添加学生",@"批量添加学生"];
}

-(CGFloat)maxAddMenuLength
{
    UIFont *newFont = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    UIFontDescriptor *ctfFont = newFont.fontDescriptor;
    NSNumber *fontString = [ctfFont objectForKey:@"NSFontSizeAttribute"];
    CGFloat length = 0;
    for(NSString *s in [self addMenuArray])
    {
        length = s.length > length ? s.length : length;
    }
    return length * [fontString doubleValue];
}

- (IBAction)Add:(UIBarButtonItem *)sender
{
    [FTPopOverMenu showFromSenderFrame:CGRectMake(self.view.frame.size.width - 45, 20, 40, 40)
                         withMenuArray:[self addMenuArray]
                             doneBlock:^(NSInteger selectedIndex) {
                                 if([[self addMenuArray][selectedIndex] isEqualToString:@"添加学生"])
                                 {
                                     UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                     UIViewController *addStudent = [mainStroyBoard instantiateViewControllerWithIdentifier:@"addStudent"];
                                     [self.navigationController pushViewController:addStudent animated:YES];
                                 } else if ([[self addMenuArray][selectedIndex] isEqualToString:@"创建班级"])
                                 {
                                     UIStoryboard *mainStroyBoard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                     UIViewController *addClass = [mainStroyBoard instantiateViewControllerWithIdentifier:@"addClass"];
                                     [self.navigationController pushViewController:addClass animated:YES];
                                 }
                                 
                             } dismissBlock:^{
                                 
                             }];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self initializeFetchedResultsController];
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;

    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    self.navigationItem.leftBarButtonItem = self.editButtonItem;
    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
    //[self.navigationItem setRightBarButtonItem:[[UIBarButtonItem alloc]initWithTitle:@"" style:UIBarButtonItemStyleDone target:self action:nil]];
    
    [self initializeFetchedResultsController];
    [self configurePopMenu];
}

-(void)configurePopMenu
{
    FTPopOverMenuConfiguration *configuration = [FTPopOverMenuConfiguration defaultConfiguration];
    configuration.menuWidth = [self maxAddMenuLength];
    configuration.textAlignment = NSTextAlignmentCenter;
    configuration.allowRoundedArrow = YES;// Default is 'NO', if sets to 'YES', the arrow will be drawn with round corner.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma makr - initializeFetchedResultsController

-(void)initializeFetchedResultsController
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:TEACHINGCLASS_ENTITYNAME];
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:TEACHING_CLASS_NAME ascending:YES];
    [request setSortDescriptors:@[descriptor]];
    self.fetchedResultsController = [[NSFetchedResultsController alloc]initWithFetchRequest:request
                                                                      managedObjectContext:self.context
                                                                        sectionNameKeyPath:nil
                                                                                 cacheName:nil];
    self.fetchedResultsController.delegate = self;
    NSError *error = nil;
    if(![self.fetchedResultsController performFetch:&error]) {
        NSLog(@"Failed to initialize FetchedResultsController: %@\n%@", [error localizedDescription], [error userInfo]);
        abort();
    }
}


#pragma mark - Table view data source

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    TeachingClass *oneClass = [[self fetchedResultsController] objectAtIndexPath:indexPath];
    
    // Populate cell from the NSManagedObject instance
    cell.textLabel.text = oneClass.name;
    cell.detailTextLabel.text = [NSString stringWithFormat:@"总人数: %lu", oneClass.students.count];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
//#warning Incomplete implementation, return the number of sections
    return [self.fetchedResultsController.sections count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of rows
    id<NSFetchedResultsSectionInfo> sectionInfo = self.fetchedResultsController.sections[section];
    return [sectionInfo numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ClassCell" forIndexPath:indexPath];
    
    // Configure the cell...
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}


#pragma mark - NSFetchedResultsControllerDelegate
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [[self tableView] beginUpdates];
}
- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [[self tableView] insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [[self tableView] deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeMove:
        case NSFetchedResultsChangeUpdate:
            break;
    }
}
- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [[self tableView] insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeDelete:
            [[self tableView] deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeUpdate:
            [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
        case NSFetchedResultsChangeMove:
            [[self tableView] deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [[self tableView] insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}
- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [[self tableView] endUpdates];
}


// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}



// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        
        //!!Note:The delete rule of class is Cascade
        [GlobalFunctions deletStudentsImagesIn:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        [self.context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        [self.context save:nil];
        //[tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}


/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    TeachingClass *selectedClass = [self.fetchedResultsController objectAtIndexPath:indexPath];
    NSString *className = selectedClass.name;
    //NSLog(@"fectedResult = %@", className);
    StudentsOfClassCollectionViewController *scvc = [segue destinationViewController];
    scvc.className = className;
}


@end
