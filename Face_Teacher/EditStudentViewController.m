//
//  EditStudentViewController.m
//  Face_Teacher
//
//  Created by jackstevenson on 17/5/30.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import "EditStudentViewController.h"
#import "GlobalSetting.h"
#import "TeachingClass+CoreDataClass.h"
#import "AppDelegate.h"
#import "GlobalFunctions.h"
#import "FacePlusPlusTools.h"

@interface EditStudentViewController () <FacePlusPlusDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *photoView;
@property (weak, nonatomic) IBOutlet UITextField *nameTextView;
@property (weak, nonatomic) IBOutlet UIButton *classTextView;
@property (weak, nonatomic) IBOutlet UITextField *IDNumberTextView;
@property (weak, nonatomic) IBOutlet UILabel *face_token;
@property (strong, nonatomic) NSManagedObjectContext *context;
@property (strong, nonatomic) AppDelegate *appDelegate;
@property (strong, nonatomic) FacePlusPlusTools *facePlusPlusTools;
@property (strong, nonatomic) UILabel *label;
@property (strong, nonatomic) UIActivityIndicatorView *uai;
@property (strong, nonatomic) NSString *imagePath;
@property (weak, nonatomic) IBOutlet UILabel *attendance;
@end

@implementation EditStudentViewController

#pragma mark - PropertySetting
-(void)setStudent:(Student *)student
{
    _student = student;
    self.context = student.managedObjectContext;
}

- (AppDelegate *)appDelegate
{
    if(!_appDelegate)
    {
        _appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    }
    return _appDelegate;
}

- (void)fillNewDataToStuent
{
    self.student.name = self.nameTextView.text;
    self.student.idNumber = self.IDNumberTextView.text;
}

- (FacePlusPlusTools *)facePlusPlusTools
{
    if(!_facePlusPlusTools)
    {
        _facePlusPlusTools = [[FacePlusPlusTools alloc]init];
        _facePlusPlusTools.delegate = self;
    }
    return _facePlusPlusTools;
}

- (NSString *)imagePath
{
    if(!_imagePath)
    {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        _imagePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:self.student.idNumber];
    }
    return _imagePath;
}

#pragma make - handleNetworkEvent

- (void)removeFaceSuccess:(NSData *)data
{
    self.uai = nil;
    self.label = nil;
    @try {
        [self.context deleteObject:self.student];
        [self.context save:nil];
        NSError *error;
        [[NSFileManager defaultManager] removeItemAtPath:self.imagePath error:&error];
        if(error) @throw [[NSException alloc]initWithName:[error localizedDescription] reason:[error localizedFailureReason] userInfo:nil];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self dismissViewControllerAnimated:YES completion:^{
                [self.delegate reloadCollectionView];
            }];
        });
    } @catch (NSException *exception) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [GlobalFunctions showErrorAlertTo:self withTitle:exception.description Message:exception.reason buttonName:@"OK"];
        });
        [self.context rollback];
    }
}

- (void)dealWithError:(NSData *)data response:(NSURLResponse *)response andError:(NSError *)error
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.uai stopAnimating];
        self.label.hidden = YES;
    });
    if(error)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
                    [GlobalFunctions showErrorAlertTo:self withTitle:[response description] Message:[error localizedDescription] buttonName:@"OK"];
        });
    }
    else if(data)
    {
        NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSArray *reasons = [result valueForKeyPath:@"failure_detail.reason"];
        if([[reasons firstObject] isEqualToString:@"INVALID_FACE_TOKEN"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [GlobalFunctions showErrorAlertTo:self withTitle:@"警告" Message:@"FaceToken不存在于服务器" buttonName:@"本地删除" confirm:^{
                    [self removeFaceSuccess:nil];
                } withCancelButton:@"取消" canceled:^{
                    
                }];
            });
        }
        NSString *message = [result valueForKey:@"error_message"];
        if(message)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [GlobalFunctions showErrorAlertTo:self withTitle:@"错误" Message:message buttonName:@"OK"];
            });
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [GlobalFunctions showErrorAlertTo:self withTitle:[response description] Message:@"未知错误" buttonName:@"OK"];
        });
    }
}

#pragma mark - UIEvent
- (IBAction)cancel:(UIBarButtonItem *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)done:(UIBarButtonItem *)sender {
    @try {
        [self fillNewDataToStuent];
        [self.appDelegate saveContext];
        [self dismissViewControllerAnimated:YES completion:^{
            [self.delegate reloadCollectionView];
        }];
    } @catch (NSException *exception) {
        [GlobalFunctions showErrorAlertTo:self withTitle:@"错误" Message:exception.description buttonName:@"OK"];
        [self.context rollback];
    }
}
- (IBAction)deleteStudent:(UIButton *)sender {
    self.uai = [[UIActivityIndicatorView alloc]init];
    self.uai.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    self.uai.center = self.view.center;
    self.label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 200, 50)];
    self.label.center = CGPointMake(self.uai.center.x, self.uai.center.y + 50);
    self.label.text = @"联系服务器中。。。";
    [self.view addSubview:self.uai];
    [self.view addSubview:self.label];
    [self.uai startAnimating];
    @try {
        if(self.face_token.text == nil || [self.face_token.text length] == 0)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.uai stopAnimating];
                self.label.text = @"";
                [GlobalFunctions showErrorAlertTo:self withTitle:@"警告" Message:@"FaceToken为空" buttonName:@"本地删除" confirm:^{
                    [self removeFaceSuccess:nil];
                } withCancelButton:@"取消" canceled:^{
                }];
            });
        }
        else
        {
            [self.facePlusPlusTools removeFace:self.student.face_token InFaceSet:faceplusplus_api_faceset_token];
        }
    } @catch (NSException *exception) {
        [GlobalFunctions showErrorAlertTo:self withTitle:@"错误" Message:exception.description buttonName:@"OK"];
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.context = self.student.managedObjectContext;
    self.photoView.image = [UIImage imageWithContentsOfFile:self.imagePath];
    self.nameTextView.text = self.student.name;
    [self.classTextView setTitle:self.student.ofClass.name forState:UIControlStateNormal];
    self.IDNumberTextView.text = self.student.idNumber;
    self.IDNumberTextView.enabled = false;
    self.face_token.text = self.student.face_token;
    @try {
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:RECORD_ENTITYNAME];
        NSArray *allRecords = [self.context executeFetchRequest:request error:nil];
        self.attendance.text = [NSString stringWithFormat:@"%lu/%lu", self.student.records.count, allRecords.count];
    } @catch (NSException *exception) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [GlobalFunctions showErrorAlertTo:self withTitle:exception.name Message:exception.description buttonName:@"OK"];
        });
        self.attendance.text = [NSString stringWithFormat:@"%lu", self.student.records.count];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
