//
//  AddStudentViewController.m
//  FaceTeachingRoom_Teacher
//
//  Created by jackstevenson on 2017/3/10.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import "AddStudentViewController.h"
#import "FacePlusPlusTools.h"
#import "FaceDetector.h"

@interface AddStudentViewController () <UIPickerViewDelegate, UIPickerViewDataSource, FacePlusPlusDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *faceFetureImageView;
@property (nonatomic) UIImage *studentPhoto;
@property (weak, nonatomic) IBOutlet UIImageView *photoImageView;
@property (weak, nonatomic) IBOutlet UITextField *nameTextView;
@property (weak, nonatomic) IBOutlet UITextField *studentNumberTextView;
@property (weak, nonatomic) IBOutlet UIPickerView *classPickerView;
@property (weak, nonatomic) IBOutlet UIButton *confirmSelectedClassButton;
@property (weak, nonatomic) IBOutlet UILabel *classLabel;
@property (strong, nonatomic) NSArray *classArray;
@property (strong, nonatomic) AppDelegate *appDeleget;
@property (strong, nonatomic) NSManagedObjectContext *context;
@property (strong, nonatomic) TeachingClass *selectedClass;
@property (nonatomic) BOOL calssIsSelected;
@property (nonatomic, strong) FacePlusPlusTools *faceTools;
@property (nonatomic, strong) FaceDetector *faceDetector;
@property (nonatomic) BOOL haveSetFaceFeture;
@property (nonatomic, strong) NSString *student_face_token;
@property (weak, nonatomic) IBOutlet UISwitch *isAddToServerSwitch;
@property (nonatomic, strong) FaceRecognizer *faceRecognizer;
@property (nonatomic, strong) NSString *faceVector_JsonStr;
@end

@implementation AddStudentViewController

#define WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define HEIGHT ([[UIScreen mainScreen] bounds].size.height)

#pragma mark - propertyEvents
-(FaceDetector *)faceDetector
{
    if(!_faceDetector)
    {
        _faceDetector = [[FaceDetector alloc]init];
    }
    return _faceDetector;
}

-(FacePlusPlusTools *)faceTools
{
    if(!_faceTools)
    {
        _faceTools = [[FacePlusPlusTools alloc]init];
        [_faceTools setDelegate:self];
    }
    return _faceTools;
}

-(AppDelegate *)appDeleget
{
    if(!_appDeleget) _appDeleget = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    return _appDeleget;
}

-(NSManagedObjectContext *)context
{
    if(!_context)
    {
        NSPersistentContainer *persistentContainer = [self.appDeleget persistentContainer];
        _context =  persistentContainer.viewContext;
    }
    return _context;
}

-(FaceRecognizer *)faceRecognizer
{
    if(!_faceRecognizer) _faceRecognizer = [[FaceRecognizer alloc]init];
    return _faceRecognizer;
}

#pragma mark - handle with network requests

-(void)dealWithError:(NSData *)data response:(NSURLResponse *)response andError:(NSError *)error
{
    dispatch_async(dispatch_get_main_queue(), ^{
        if(error)
        {
            [GlobalFunctions showErrorAlertTo:self withTitle:[NSString stringWithFormat:@"ErrorCode:%ld", (long)error.code] Message:error.localizedDescription buttonName:@"OK"];
        }
        [GlobalFunctions showErrorAlertTo:self withTitle:[NSString stringWithFormat:@"%ld", (long)((NSHTTPURLResponse *)response).statusCode] Message:[[NSString alloc]initWithData:data encoding:kCFStringEncodingUTF8] buttonName:@"OK"];
    });
}

-(void)finishAddFaceToServer:(NSData *)data usingFaceToken:(NSString *)face_token
{
    NSDictionary *obj = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSString *face_added_count = [obj valueForKeyPath:@"face_added"];
    face_token = [face_token stringByReplacingOccurrencesOfString:@" " withString:@""];
    face_token = [face_token stringByReplacingOccurrencesOfString:@"(" withString:@""];
    face_token = [face_token stringByReplacingOccurrencesOfString:@")" withString:@""];
    self.student_face_token = face_token;
    NSString *message = [NSString stringWithFormat:@"face_token:%@ addedToServer, count = %@", face_token, face_added_count];
    dispatch_async(dispatch_get_main_queue(), ^{
           [GlobalFunctions showSuccessAlertTo:self withTitle:@"添加至服务器成功" Message:message buttonName:@"Yes"];
    });
}

- (void)removeFaceSuccess:(NSData *)data
{
    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSString *removedCountStr = [result valueForKey:@"face_removed"];
    __block NSString *message;
    dispatch_async(dispatch_get_main_queue(), ^{
        @try {
            int count = [removedCountStr intValue];
            if(count)
            {
                message = [NSString stringWithFormat:@"face_removed = %@", removedCountStr];
                [GlobalFunctions showSuccessAlertTo:self withTitle:@"移除成功" Message:message buttonName:@"OK"];
            }
            else
            {
                @throw [NSException exceptionWithName:@"删除失败" reason:@"assert(count > 0) == false" userInfo:nil];
            }
        } @catch (NSException *exception) {
            [GlobalFunctions showErrorAlertTo:self withTitle:exception.name Message:exception.description buttonName:@"OK" confirm:^(){
                self.isAddToServerSwitch.on = YES;
            } withCancelButton:nil canceled:nil];
        }
    });
}

#pragma mark - 添加人脸到server的face_set
- (IBAction)addFaceToServer:(UISwitch *)sender
{
    @try {
        if(sender.isOn)
        {
            NSLog(@"add");
            [self.faceTools addFace:self.faceFetureImageView.image ToFaceSet:nil];
        }
        else if(self.student_face_token)
        {
            NSLog(@"removed");
            [self.faceTools removeFace:self.student_face_token InFaceSet:faceplusplus_api_faceset_token];
        }
    } @catch (NSException *exception) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [GlobalFunctions showSuccessAlertTo:self withTitle:@"错误" Message:[exception description] buttonName:@"OK"];
        });
    }
}




- (IBAction)setOriginalImage:(UIButton *)sender
{
    [self.faceFetureImageView setImage:self.studentPhoto];
}

#pragma mark - 从学生照片中检测人脸显示到右端

-(void)setStudentPhoto:(UIImage *)studentPhoto
{
    _studentPhoto = studentPhoto;
    self.faceFetureImageView.image = _studentPhoto;
    [self detecFace];
}
- (IBAction)tryDetectAgain:(UIButton *)sender
{
    [self detecFace];
}

//当学生照片设置好时检测面部特征
-(void)detecFace
{
    if(self.studentPhoto == nil) return;
    UIView *maskView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.faceFetureImageView.frame.size.width, _faceFetureImageView.frame.size.height)];
    maskView.backgroundColor = [UIColor whiteColor];
    maskView.alpha = 1.0f;
    UIActivityIndicatorView *aiView = [[UIActivityIndicatorView alloc]initWithFrame:CGRectMake(0, 0, self.faceFetureImageView.frame.size.width, self.faceFetureImageView.frame.size.height)];
    aiView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    aiView.backgroundColor = [UIColor darkGrayColor];
    aiView.alpha = 0.7f;
    aiView.center = maskView.center;
    aiView.tintColor = [UIColor blueColor];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.faceFetureImageView.frame.size.width, 50)];
    label.text = @"(最大)人脸检测中...";
    label.textAlignment = NSTextAlignmentCenter;
    label.center = maskView.center;
    
    [maskView addSubview:label];
    [maskView addSubview:aiView];
    [self.faceFetureImageView addSubview:maskView];
    [aiView startAnimating];
    dispatch_queue_t getFacefetureQueue = dispatch_queue_create("getFacefetureQueue", NULL);
    dispatch_async(getFacefetureQueue, ^{
        UIImage *face = [self.faceDetector cvGetBigestFaceInImage:self.photoImageView.image];
        dispatch_async(dispatch_get_main_queue(), ^{
            [self finishGetFaceFetureInImage:face];
        });
    });
}

-(void)finishGetFaceFetureInImage:(UIImage *)face;
{
    for(UIView *subview in self.faceFetureImageView.subviews)
    {
        [subview removeFromSuperview];
    }
    if(!face)
    {
        self.faceFetureImageView.image = [UIImage imageNamed:@"头像"];
        [GlobalFunctions showErrorAlertTo:self withTitle:@"错误" Message:@"未在您的照片中检测到人脸,请重新拍照" buttonName:@"知道了"];
    }
    else
    {
        @try
        {
            MLMultiArray *vector = [self.faceRecognizer imageToVectorOfMLArray:face];
            self.faceVector_JsonStr = [self.faceRecognizer vectorToJsonStr:vector];
        }@catch(NSException *exception)
        {
            
        }
        self.faceFetureImageView.contentMode = UIViewContentModeScaleAspectFit;
        self.faceFetureImageView.image = face;
        self.haveSetFaceFeture = YES;
    }

}

//-(void)addFacetoFacSet
//{
//    UIView *maskView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
//    maskView.backgroundColor = [UIColor whiteColor];
//    maskView.alpha = 0.8f;
//    UIView *addingFaceStatusView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 300, 300)];
//    addingFaceStatusView.center = self.view.center;
//    addingFaceStatusView.backgroundColor = [UIColor grayColor];
//    UIActivityIndicatorView *aiView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    aiView.center = CGPointMake(addingFaceStatusView.frame.size.width / 2, addingFaceStatusView.frame.size.height / 2);
//    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 10, addingFaceStatusView.frame.size.width, 50)];
//    label.text = @"添加人脸至数据库中";
//    label.textAlignment = NSTextAlignmentCenter;
//    [addingFaceStatusView addSubview:label];
//    [addingFaceStatusView addSubview:aiView];
//    [self.view addSubview:maskView];
//    [self.view addSubview:addingFaceStatusView];
//    [aiView startAnimating];
//    UIImage *face = [self.faceDetector cvGetBigestFaceInImage: self.photoImageView.image];
//    UIImageView *facePreView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, addingFaceStatusView.frame.size.width, addingFaceStatusView.frame.size.height)];
//    facePreView.contentMode = UIViewContentModeScaleAspectFit;
//    facePreView.image = face;
//    [addingFaceStatusView addSubview:facePreView];
//}


-(NSArray *)classArray
{
    if(!_classArray)
    {
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:TEACHINGCLASS_ENTITYNAME];
        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:TEACHING_CLASS_NAME ascending:YES];
        [request setSortDescriptors:@[descriptor]];
        NSError *error;
        _classArray = [self.context executeFetchRequest:request error:&error];
        if(![_classArray count])
        {
            NSLog(@"fetchError: %@", error);
        }
    }
    return _classArray;
}


#pragma mark - UIEvents

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.classPickerView.hidden = YES;
    self.confirmSelectedClassButton.hidden = YES;
    self.calssIsSelected = NO;
    //更改Item and barItem按钮颜色
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
//#warning 临时设置为YES，用于测试
    //self.haveSetFaceFeture = YES;
    self.haveSetFaceFeture = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -- PickerView
- (IBAction)confrimClassForPickerView:(UIButton *)sender
{
    self.classPickerView.hidden = YES;
    self.confirmSelectedClassButton.hidden = YES;
    for(UIView *view in self.view.subviews)
    {
        if(view != self.classPickerView && view != self.confirmSelectedClassButton) view.hidden = NO;
    }
    self.classLabel.text = [self.classArray[[self.classPickerView selectedRowInComponent:0]] valueForKey:TEACHING_CLASS_NAME];
    self.calssIsSelected = YES;
    NSInteger selectedIndex = [self.classPickerView selectedRowInComponent:0];
    if([[self.classArray[selectedIndex] valueForKey:TEACHING_CLASS_NAME] length])
    {
        self.classLabel.text = [self.classArray[selectedIndex] valueForKey:TEACHING_CLASS_NAME];
        self.selectedClass = self.classArray[selectedIndex];
    }
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.classArray.count;
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [self.classArray[row] valueForKey:TEACHING_CLASS_NAME];
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if([[self.classArray[row] valueForKey:TEACHING_CLASS_NAME] length])
    {
        self.classLabel.text = [self.classArray[row] valueForKey:TEACHING_CLASS_NAME];
        self.selectedClass = self.classArray[row];
    }
}

- (IBAction)selectClass:(UIButton *)sender
{
    if(![self.classArray count])
    {
        [GlobalFunctions showErrorAlertTo:self withTitle:@"班级为空" Message:@"请先创建班级" buttonName:@"确认"];
        return;
    }
    for(UIView *view in self.view.subviews)
    {
        view.hidden = YES;
    }
    self.classPickerView.hidden = NO;
    self.confirmSelectedClassButton.hidden = NO;
    self.classPickerView.delegate = self;
    self.classPickerView.dataSource = self;
    self.classPickerView.backgroundColor = [UIColor whiteColor];
    self.classPickerView.showsSelectionIndicator = YES;
}
- (IBAction)takeStudentPhoto:(UIButton *)sender
{
    UIImagePickerController *uiipc = [[UIImagePickerController alloc]init];
    uiipc.delegate = self;
    uiipc.mediaTypes = @[(NSString *)kUTTypeImage];
    if(sender.tag == 0) uiipc.sourceType = UIImagePickerControllerSourceTypeCamera;
    if(sender.tag == 1) uiipc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    uiipc.allowsEditing = YES;
    [self presentViewController:uiipc animated:YES completion:NULL];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *image = info[UIImagePickerControllerEditedImage];
    if(!image) image = info[UIImagePickerControllerOriginalImage];
    self.studentPhoto = image;
    [self.photoImageView setImage:self.studentPhoto];
    [self dismissViewControllerAnimated:YES completion:NULL];
}




#pragma mark - addStudentFunctions

- (IBAction)confirmAddStudent:(UIButton *)sender
{
    if([self.nameTextView.text length] && [self.studentNumberTextView.text length] && self.calssIsSelected)
    {
        //键值唯一性检查
        NSError *error = nil;
        @try {
            [GlobalFunctions value:self.studentNumberTextView.text ofKey:STUDENT_PROPERTY_ID isExistInEntity:STUDENT_ENTITYNAME Error:&error];
            if(error) @throw [NSException exceptionWithName:[error localizedDescription]
                                                     reason:[NSString stringWithFormat:@"%d", UNIQUEKEY_CONFLICT]
                                                   userInfo:[error userInfo]];
            [self saveStudent:error];
            [self showSaveSuccessed];
        } @catch (NSException *exception) {
            NSLog(@"%@", [error localizedDescription]);
            if([[exception reason] isEqualToString:[NSString stringWithFormat:@"%d", UNIQUEKEY_CONFLICT]])
            {
                [self handleUniqueKeyConflict:nil];
            }
            else
            {
                [self showWarning:error];
            }
        }
    } else {
        [GlobalFunctions showErrorAlertTo:self withTitle:@"Error" Message:@"班级学号姓名均不能为空" buttonName:@"确认"];
    }
}

-(void)handleUniqueKeyConflict:(NSError *)customError
{
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    [alert addButton:@"覆盖" actionBlock:^{
        //[self saveStudent:error];
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:STUDENT_ENTITYNAME];
        request.predicate = [NSPredicate predicateWithFormat:@"idNumber = %@", self.studentNumberTextView.text];
        NSArray *students = [self.context executeFetchRequest:request error:nil];
        Student *student = [students firstObject];
        student.name = self.nameTextView.text;
        student.idNumber = self.studentNumberTextView.text;
        student.ofClass = self.selectedClass;
        student.face_token = self.student_face_token;
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:self.studentNumberTextView.text];
        // Save image.
        [UIImagePNGRepresentation(self.photoImageView.image) writeToFile:filePath atomically:YES];
        
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alert addButton:@"取消" actionBlock:^{
        //do nothing
    }];
    [alert showError:[customError localizedDescription]
              subTitle:[NSString stringWithFormat:@"该学号已经存在，若确需更新请选择覆盖"]
      closeButtonTitle:nil duration:0.0f];
}

-(void)showSaveSuccessed
{
    SCLAlertView *alert = [[SCLAlertView alloc] initWithNewWindow];
    [alert addButton:@"确认" actionBlock:^{
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alert showSuccess:@"恭喜" subTitle:@"添加成功" closeButtonTitle:nil duration:0.0f];
}

-(void)showWarning:(NSError *)error
{
    [GlobalFunctions showErrorAlertTo:self withTitle:[error description] Message:[error localizedFailureReason] buttonName:@"确认"];
}

-(Student *)addStudentInfoToContext
{
    Student *aStudent = [[Student alloc]initWithContext:self.context];
    aStudent.name = self.nameTextView.text;
    aStudent.idNumber = self.studentNumberTextView.text;
    aStudent.ofClass = self.selectedClass;
    aStudent.face_token = self.student_face_token;
    aStudent.face_vector = self.faceVector_JsonStr;
    NSLog(@"save vector:%@", self.faceVector_JsonStr);
    return aStudent;
}

-(BOOL)saveStudent:(NSError *)error
{
    Student *aStudent = [self addStudentInfoToContext];
    @try {
        [self.appDeleget saveContextWithReturnValue:&error];
        if(error) @throw [NSException exceptionWithName:error.localizedDescription
                                                 reason:error.localizedFailureReason
                                               userInfo:error.userInfo];
        // Create path.
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:aStudent.idNumber];
        
        // Save image.
        [UIImagePNGRepresentation(self.photoImageView.image) writeToFile:filePath atomically:YES];
    } @catch (NSException *exception) {
        NSLog(@"add student error %@", [exception description]);
        return NO;
    }
    return YES;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
