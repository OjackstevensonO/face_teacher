//
//  ClassManagerTableViewController.h
//  FaceTeachingRoom_Teacher
//
//  Created by jackstevenson on 2017/3/10.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "FTPopOverMenu.h"
#import "AppDelegate.h"
#import "GlobalSetting.h"
#import "GlobalFunctions.h"
#import "TeachingClass+CoreDataClass.h"
@interface ClassManagerTableViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@end
