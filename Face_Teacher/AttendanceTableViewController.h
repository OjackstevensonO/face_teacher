//
//  AttendanceTableViewController.h
//  Face_Teacher
//
//  Created by jackstevenson on 17/6/10.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Record+CoreDataClass.h"

@interface AttendanceTableViewController : UITableViewController
@property (nonatomic, strong) Record *record;
@end
