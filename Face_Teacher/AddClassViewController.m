//
//  AddClassViewController.m
//  Face_Teacher
//
//  Created by jackstevenson on 17/3/18.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import "AddClassViewController.h"

@interface AddClassViewController ()
@property (weak, nonatomic) IBOutlet UITextField *className;
@end

@implementation AddClassViewController

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (IBAction)confirmAddAClass:(UIButton *)sender
{
    if([self.className.text length])
    {
        AppDelegate *appDeleget = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSPersistentContainer *container = [appDeleget persistentContainer];
        NSManagedObjectContext *context = [container viewContext];
        @try {
            NSError *error = nil;
            [GlobalFunctions value:self.className.text ofKey:TEACHING_CLASS_NAME isExistInEntity:TEACHINGCLASS_ENTITYNAME Error:&error];
            if(error) @throw [NSException exceptionWithName:[error localizedDescription]
                                                     reason:[error localizedFailureReason]
                                                   userInfo:[error userInfo]];
            TeachingClass *newClass = [[TeachingClass alloc]initWithContext:context];
            newClass.name = self.className.text;
            [appDeleget saveContextWithReturnValue:&error];
            [GlobalFunctions showSuccessAlertTo:self withTitle:@"祝贺" Message:@"添加班级成功" buttonName:@"确认"];
        } @catch (NSException *exception) {
            if([[exception.userInfo valueForKey:NSLocalizedFailureReasonErrorKey] isEqualToString:[NSString stringWithFormat:@"%d", UNIQUEKEY_CONFLICT]])
            {
                [GlobalFunctions showErrorAlertTo:self withTitle:@"错误" Message:@"班级已存在" buttonName:@"确认"];
            }
            else
            {
                [GlobalFunctions showErrorAlertTo:self withTitle:[exception reason] Message:[exception description] buttonName:@"确认"];
            }
        }
    }
    else
    {
        [GlobalFunctions showErrorAlertTo:self withTitle:@"错误" Message:@"班级名不能为空" buttonName:@"确认"];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //由于navigationController的影响，重新定义textView的位置。
    self.className.frame = CGRectMake(self.className.frame.origin.x, self.className.frame.origin.y + self.navigationController.navigationBar.frame.origin.y + self.navigationController.navigationBar.frame.size.height, self.className.frame.size.width, self.className.frame.size.height);
    
    //更改Item and barItem按钮颜色
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
