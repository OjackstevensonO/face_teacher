//
//  AddClassViewController.h
//  Face_Teacher
//
//  Created by jackstevenson on 17/3/18.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import "ViewController.h"
#import "AppDelegate.h"
#import "TeachingClass+CoreDataClass.h"
#import "GlobalSetting.h"
#import "GlobalFunctions.h"
@interface AddClassViewController : ViewController

@end
