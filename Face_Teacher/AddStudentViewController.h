//
//  AddStudentViewController.h
//  FaceTeachingRoom_Teacher
//
//  Created by jackstevenson on 2017/3/10.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "AppDelegate.h"
#import "Student+CoreDataClass.h"
#import "GlobalFunctions.h"
#import "Teacher+CoreDataClass.h"
#import "FaceRecognizer.h"
@interface AddStudentViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>
@end
