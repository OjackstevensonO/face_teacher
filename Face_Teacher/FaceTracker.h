//
//  FaceTracker.h
//  Face_Teacher
//
//  Created by jackstevenson on 17/4/23.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//



//#include "CompressiveTracker.h"
#import "CMT.h"
#import <opencv2/opencv.hpp>
#import <opencv2/tracking.hpp>
#import <opencv2/imgproc.hpp>
#import <opencv2/imgcodecs/ios.h>
#import <Foundation/Foundation.h>
#import <CoreImage/CoreImage.h>
#import <UIKit/UIKit.h>

using namespace std;

@interface FaceTracker : NSObject
-(void)updateFrame:(UIImage *)frameImage;//输入新的一帧，更新模型并产生物体所在的新的位置
-(CGRect)getTrackingRectBox;//获取被跟踪物体当前的位置
-(void)startWithFrame:(UIImage *)frameImage andBox:(CGRect)objectBox;//初始化第一帧图像，和所需跟踪的物体的位置
-(void)stopTracker;
-(BOOL)trackerIsTracking;
-(BOOL)isNewFace:(CGRect)faceRect;
-(BOOL)objectIsOutOfbound;
@end
