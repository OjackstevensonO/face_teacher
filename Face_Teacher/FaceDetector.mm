//
//  FaceDetector.m
//  Face_Teacher
//
//  Created by jackstevenson on 17/4/28.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//
#import <opencv2/face.hpp>
#import <opencv2/opencv.hpp>
#import <opencv2/core.hpp>
#import <opencv2/imgcodecs/ios.h>

#import "FaceDetector.h"
#import "UIImage+UIImageCut.h"


@interface FaceDetector()
{
    //cv::CascadeClassifier _cvFaceDetector;
}
@property (nonatomic) cv::CascadeClassifier cvFaceDetector;
@property (strong, nonatomic) CIDetector *faceDetector;
@end
@implementation FaceDetector

const double scale_factor = 1.0;

-(cv::CascadeClassifier)cvFaceDetector
{
    if(_cvFaceDetector.empty())
    {
        //lbpcascade_frontalface_improved.xml, haarcascade_frontalface_alt_tree.xml
        NSString *faceCascadePath = [[NSBundle mainBundle] pathForResource:@"lbpcascade_frontalface_improved" ofType:@"xml"];
        const CFIndex CASCADE_NAME_LEN = 2048;
        char *CASCADE_NAME = (char *) malloc(CASCADE_NAME_LEN);
        CFStringGetFileSystemRepresentation( (CFStringRef)faceCascadePath, CASCADE_NAME, CASCADE_NAME_LEN);
        _cvFaceDetector.load(CASCADE_NAME);
        free(CASCADE_NAME);
    }
    return _cvFaceDetector;
}

- (CIDetector *)faceDetector
{
    if(!_faceDetector)
    {
        NSDictionary * param = [NSDictionary dictionaryWithObject:CIDetectorAccuracyHigh forKey:CIDetectorAccuracy];
        _faceDetector = [CIDetector detectorOfType:CIDetectorTypeFace context:nil options:param];
    }
    return _faceDetector;
}

-(CGRect)scale:(CGRect)rect to:(double)time
{
    CGRect newRect = rect;
    newRect.size.width *= time;
    newRect.size.height *= time;
    newRect = CGRectOffset(newRect, rect.size.width * (1 - time) / 2, rect.size.height * (1 - time) / 2);
    return newRect;
}

-(UIImage *)getBigestFcaeInImage:(UIImage *)image
{
    NSData *data = UIImagePNGRepresentation(image);
    UIImage *face;
    CGRect faceRect = CGRectZero;
    CIImage *ciimage = [CIImage imageWithData:data];
    NSArray *fetures =  [self.faceDetector featuresInImage:ciimage options:nil];
    for(CIFeature *f in fetures)
    {
        if(faceRect.size.width * faceRect.size.height < [f bounds].size.width * [f bounds].size.height)
        {
            faceRect = [f bounds];
        }
    }
    face = [[UIImage imageWithData:data] cutImageatRect:faceRect];
    return face;
}

-(UIImage *)cvGetBigestFaceInImage:(UIImage *)image
{
    /*
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);
    cv::Mat grayImage;
    cv::cvtColor(imageMat, grayImage, CV_RGB2GRAY);
    std::vector< cv::Rect > faces;
    faces.clear();
    self.cvFaceDetector.detectMultiScale(grayImage, faces);
    cv::Rect face = cv::Rect(0, 0, 0, 0);
     for(unsigned long i = 0; i < faces.count; i++)
     {
     if(face.area() < (NSValue *)faces[i])
     {
     face = faces[i];
     }
     }
    return [image cutImageatRect:CGRectMake(face.x, face.y, face.size().width, face.size().height)];
     */
    NSArray *faces = [self detectFacesInImage:image];
    CGRect face = CGRectZero;
    for(NSValue *f in faces)
    {
        CGRect rect = [f CGRectValue];
        if(face.size.width * face.size.height < rect.size.width * rect.size.height) face = rect;
    }
    if(faces == nil || faces.count == 0) return nil;
    return [image cutImageatRect:face];
}


-(NSArray *)detectFacesInImage:(UIImage *)image
{
    NSMutableArray *temp = [[NSMutableArray alloc]init];
    cv::Mat imageMat;
    UIImageToMat(image, imageMat);
    cv::Mat grayImage;
    cv::cvtColor(imageMat, grayImage, CV_RGB2GRAY);
    std::vector< cv::Rect > faces;
    faces.clear();
    self.cvFaceDetector.detectMultiScale(grayImage, faces);
    for(int i = 0; i < faces.size(); i++)
    {
        CGRect rect = CGRectMake(faces[i].tl().x, faces[i].tl().y, faces[i].size().width, faces[i].size().height);
        rect = [self scale:rect to:scale_factor];
        [temp addObject:[NSValue valueWithCGRect:rect]];
    }
    return [NSArray arrayWithArray:temp];
}

@end
