//
//  StudentsOfClassCollectionViewController.m
//  Face_Teacher
//
//  Created by jackstevenson on 17/3/28.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import "StudentsOfClassCollectionViewController.h"
#import "StudentCollectionViewCell.h"
#import "Student+CoreDataClass.h"
#import "EditStudentViewController.h"
@interface StudentsOfClassCollectionViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewController;
@property (strong, nonatomic) NSArray *students;
@end

@implementation StudentsOfClassCollectionViewController

static NSString * const reuseIdentifier = @"studentInClassCollectionViewCell";

#pragma mark - handleUIEvent

- (void)reloadCollectionView
{
    self.students = nil;
    [self.collectionViewController reloadData];
}

#pragma mark - UISetting
- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Register cell classes
    //[self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIdentifier];
    
    // Do any additional setup after loading the view.
    //更改Item and barItem按钮颜色
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.collectionViewController.delegate = self;
    self.collectionViewController.dataSource = self;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma makr - initializeFetchedResultsController

- (NSArray *)students
{
    if(!_students)
    {
        AppDelegate *appDeleget = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSPersistentContainer *container = [appDeleget persistentContainer];
        NSManagedObjectContext *context = [container viewContext];
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:STUDENT_ENTITYNAME];
        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:STUDENT_PROPERTY_ID ascending:YES];
        [request setSortDescriptors:@[descriptor]];
        request.predicate = [NSPredicate predicateWithFormat:@"ofClass.name = %@", self.className];
        //NSLog(@"class Name = %@", self.className);
        NSError *error = nil;
        _students = [context executeFetchRequest:request error:&error];
        if(!_students)
        {
            NSLog(@"student nil");
        }
    }
    return _students;
    
}





#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if([[segue destinationViewController] isKindOfClass:[EditStudentViewController class]])
    {
        EditStudentViewController *destinationVC = (EditStudentViewController *)[segue destinationViewController];
        if([[[sender superview] superview] isKindOfClass:[StudentCollectionViewCell class]])
        {
            StudentCollectionViewCell *touchedCell = (StudentCollectionViewCell *)[[sender superview] superview];
            destinationVC.student = touchedCell.student;
            destinationVC.delegate = self;
        }
        else
        {
            NSLog(@"%@", [sender superview]);
            NSLog(@"%@", [[sender superview] superview]);
            NSLog(@"superview error");
        }
    }
    else
    {
        NSLog(@"dvc error");
    }
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
//#warning Incomplete implementation, return the number of sections
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//#warning Incomplete implementation, return the number of items
    return self.students.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    StudentCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    Student *student = self.students[indexPath.row];
    if(student)
    {
        // Configure the cell
        cell.student = student;
    }
    else
    {
        NSLog(@"student nil");
    }
    return cell;
}

#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
