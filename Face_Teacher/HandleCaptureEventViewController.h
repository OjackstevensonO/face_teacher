//
//  HandleCaptureEventViewController.h
//  Face_Teacher
//
//  Created by jackstevenson on 17/3/30.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import <opencv2/opencv.hpp>
#import <opencv2/core.hpp>
//#import <opencv2/face.hpp>
#import <opencv2/imgcodecs/ios.h>
#import <opencv2/videoio/cap_ios.h>
//#import <opencv2/tracking.hpp>

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreVideo/CoreVideo.h>
#import <CoreImage/CoreImage.h>
#import <ImageIO/ImageIO.h>
#import "GlobalSetting.h"
#import "FaceRecognizer.h"


@interface HandleCaptureEventViewController : UIViewController

@end
