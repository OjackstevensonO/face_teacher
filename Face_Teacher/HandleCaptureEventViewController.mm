//
//  HandleCaptureEventViewController.m
//  Face_Teacher
//
//  Created by jackstevenson on 17/3/30.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//


#import "HandleCaptureEventViewController.h"
#import "FaceDisplayLayer.h"
#import "MultiFaceTracker.h"
#import "FacePlusPlusTools.h"
#import "FacePlusPlusDelegate.h"
#import "GlobalFunctions.h"
#import "UIImage+UIImageCut.h"
#import "OpencvTools.h"
#import "FaceDetector.h"
#import "Record+CoreDataClass.h"
#import "AppDelegate.h"
#import "RecordTable+CoreDataClass.h"


#define IS_IOS_7 ([[[UIDevice currentDevice] systemVersion] floatValue] >=7.0 ? YES : NO)
#define IS_IOS_8 ([[[UIDevice currentDevice] systemVersion] floatValue] >=8.0 ? YES : NO)
#define IS_IPHONE5 (([[UIScreen mainScreen] bounds].size.height-568)?NO:YES)
#define WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define START_POSITION ([[[UIDevice currentDevice] systemVersion] floatValue] >=7.0 ? 20 : 0)

#define FACE_DETECTOR_TIME_GAP 1

typedef enum : NSUInteger {
    PreViewLayerMode,
    ImageViewMode,
}DisplayMode;

@interface HandleCaptureEventViewController () <AVCaptureVideoDataOutputSampleBufferDelegate, FacePlusPlusDelegate>
@property (nonatomic) cv::CascadeClassifier cvFaceDetector;
@property (nonatomic) AVCaptureDeviceDiscoverySession *videoDeviceDiscoverySession;
@property (nonatomic) CGRect faceRect;
@property (strong, nonatomic) FaceDisplayLayer *faceDisplayLayer;
@property (strong, nonatomic) CIDetector *faceDetector;
@property (strong, nonatomic) FaceDetector *customFaceDetector;
@property (nonatomic) dispatch_queue_t detector_queue;
@property (nonatomic) dispatch_queue_t recognition_queue;
@property (atomic) BOOL isDetectingFace;
@property (nonatomic) FaceRecognitionTool faceRecognitionTool;
@property (strong, nonatomic) MultiFaceTracker *multiFaceTracker;
@property (nonatomic) BOOL isUsingFrontFacingCamera;
@property (nonatomic, strong) FacePlusPlusTools *facePlusPlusTool;
@property (nonatomic, strong) NSDictionary *nameOfFacetokenMapping;
@property (nonatomic, strong) UIImageView *facePreview;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) Record *record;
@property (nonatomic, strong) RecordTable *table;
@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, strong) NSArray<Student *>*students;
@property (nonatomic, strong) NSDictionary* vectorDic;
@property (nonatomic, strong) FaceRecognizer *faceRecognizer;
@end

@implementation HandleCaptureEventViewController
{
    AVCaptureSession *_captureSession;
    UIImageView *_outputImageView;
    AVCaptureVideoPreviewLayer *_captureLayer;
}

static DisplayMode currentDisplayMode = PreViewLayerMode;

- (NSManagedObjectContext *)context
{
    if(!_context)
    {
        AppDelegate *_appDeleget = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSPersistentContainer *persistentContainer = [_appDeleget persistentContainer];
            _context =  persistentContainer.viewContext;
    }
    return _context;
}

- (RecordTable *)table
{
    if(!_table)
    {
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:RECORD_TABLE_ENTITYNAME];
        NSArray *tables = [self.context executeFetchRequest:request error:nil];
        _table = [tables firstObject];
    }
    return _table;
}

- (NSArray<Student *> *)students
{
    if(!_students)
    {
        AppDelegate *appDeleget = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSPersistentContainer *container = [appDeleget persistentContainer];
        NSManagedObjectContext *context = [container viewContext];
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:STUDENT_ENTITYNAME];
        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:STUDENT_PROPERTY_ID ascending:YES];
        [request setSortDescriptors:@[descriptor]];
        //request.predicate = [NSPredicate predicateWithFormat:@"ofClass.name = %@", self.className];
        NSError *error = nil;
        _students = [context executeFetchRequest:request error:&error];
        if(!_students)
        {
            NSLog(@"student nil");
        }
    }
    return _students;
}

-(FaceRecognizer *)faceRecognizer
{
    if(!_faceRecognizer) _faceRecognizer = [[FaceRecognizer alloc]init];
    return _faceRecognizer;
}
/*
-(dispatch_queue_t)recognition_queue
{
    if(_recognition_queue == NULL) _recognition_queue = dispatch_queue_create("recognition", DISPATCH_QUEUE_SERIAL);
    return _recognition_queue;
}*/

-(NSDictionary *)vectorDic
{
    if(!_vectorDic)
    {
        NSMutableDictionary *dic = [[NSMutableDictionary alloc]init];
        for(Student *stu in self.students)
        {
            //NSDictionary *info = [[NSDictionary alloc]init];
            NSMutableDictionary *info = [[NSMutableDictionary alloc]init];
            [info setValue:stu.name forKey:@"name"];
            NSData *data = [stu.face_vector dataUsingEncoding:NSUTF8StringEncoding];
            NSArray *array = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            //NSLog(@"fetch vector from Core Data:%@", array);
            MLMultiArray *vector = [[MLMultiArray alloc]initWithShape:@[[NSNumber numberWithInteger:array.count]] dataType:MLMultiArrayDataTypeDouble error:nil];
            for(unsigned long i = 0; i < array.count; i++)
            {
                [vector setObject:array[i] atIndexedSubscript:i];
            }
            //NSLog(@"MLvector:%@", vector);
            [info setValue:vector forKey:@"vector"];
            [dic setValue:info forKey:stu.idNumber];
        }
        _vectorDic = [NSDictionary dictionaryWithDictionary:dic];
    }
    return _vectorDic;
}

- (Record *)record
{
    if(!_record)
    {
        _record = [[Record alloc]initWithContext:self.context];
        NSDate *date = [NSDate date];
        NSTimeZone *zone = [NSTimeZone systemTimeZone];
        NSInteger interval = [zone secondsFromGMTForDate: date];
        NSDate *localeDate = [date  dateByAddingTimeInterval: interval];
        _record.startTime = localeDate;
    }
    return _record;
}

-(FaceDetector *)customFaceDetector
{
    if(!_customFaceDetector)
    {
        _customFaceDetector = [[FaceDetector alloc]init];
    }
    return _customFaceDetector;
}

-(FacePlusPlusTools *)facePlusPlusTool
{
    if(!_facePlusPlusTool)
    {
        _facePlusPlusTool = [[FacePlusPlusTools alloc]init];
        [_facePlusPlusTool setDelegate:self];
    }
    return _facePlusPlusTool;
}

-(MultiFaceTracker *)multiFaceTracker
{
    if(!_multiFaceTracker)
    {
        _multiFaceTracker = [[MultiFaceTracker alloc]init];
    }
    return _multiFaceTracker;
}

- (CIDetector *)faceDetector
{
    if(!_faceDetector)
    {
        NSDictionary * param = [NSDictionary dictionaryWithObject:CIDetectorAccuracyHigh forKey:CIDetectorAccuracy];
        _faceDetector = [CIDetector detectorOfType:CIDetectorTypeFace context:nil options:param];
        //_faceDetector = [CIDetector detectorOfType:CIDetectorTypeFace context:nil options:nil];
    }
    return _faceDetector;
}

- (FaceDisplayLayer *)faceDisplayLayer
{
    if(!_faceDisplayLayer) _faceDisplayLayer = [FaceDisplayLayer layer];
    return _faceDisplayLayer;
}
-(cv::CascadeClassifier)cvFaceDetector
{
    if(_cvFaceDetector.empty())
    {
        NSString *faceCascadePath = [[NSBundle mainBundle] pathForResource:@"haarcascade_frontalface_alt_tree" ofType:@"xml"];
        const CFIndex CASCADE_NAME_LEN = 2048;
        char *CASCADE_NAME = (char *) malloc(CASCADE_NAME_LEN);
        CFStringGetFileSystemRepresentation( (CFStringRef)faceCascadePath, CASCADE_NAME, CASCADE_NAME_LEN);
        _cvFaceDetector.load(CASCADE_NAME);
        free(CASCADE_NAME);
    }
    return _cvFaceDetector;
}

- (FaceRecognitionTool)getCurFaceRecognitionScheme
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    //读取
    NSNumber *value = [defaults objectForKey:FACE_RECOGNITION_SCHEME];
    if(value == nil)
    {
        [defaults setObject:[NSNumber numberWithInt:USING_FACE_PLUS_PLUS] forKey:FACE_RECOGNITION_SCHEME];
        [defaults synchronize];
        return USING_FACE_PLUS_PLUS;
    }
    int n_scheme = [value intValue];
    switch (n_scheme) {
        case USING_FACE_PLUS_PLUS:
            return USING_FACE_PLUS_PLUS;
            break;
        case USING_FACENET:
            return USING_FACENET;
            break;
        default:
            return USING_FACE_PLUS_PLUS;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //self.faceRecognitionTool = USING_FACENET;//使用FaceNet模型作为识别方案
    //self.faceRecognitionTool = USING_FACE_PLUS_PLUS;
    self.faceRecognitionTool = [self getCurFaceRecognitionScheme];
    [self students];//load students info
    _recognition_queue = dispatch_queue_create("recognition_queue", DISPATCH_QUEUE_SERIAL);
    self.view.backgroundColor = [UIColor whiteColor];
    
    [_captureSession beginConfiguration];
    _captureSession = [[AVCaptureSession alloc] init];

    // Create a device discovery session.
    NSArray<AVCaptureDeviceType> *deviceTypes = @[AVCaptureDeviceTypeBuiltInWideAngleCamera, AVCaptureDeviceTypeBuiltInDualCamera];
    self.videoDeviceDiscoverySession = [AVCaptureDeviceDiscoverySession discoverySessionWithDeviceTypes:deviceTypes mediaType:AVMediaTypeVideo position:AVCaptureDevicePositionUnspecified];
    
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithDeviceType:AVCaptureDeviceTypeBuiltInWideAngleCamera mediaType:AVMediaTypeVideo position:AVCaptureDevicePositionBack];
    /*
    NSError *error = nil;
    if([device lockForConfiguration:&error])
    {
        device.activeVideoMinFrameDuration = CMTimeMake(1, 24);
    }
    [device unlockForConfiguration];
    */
    AVCaptureDeviceInput *captureInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:nil];
    
    AVCaptureVideoDataOutput *captureOutput = [[AVCaptureVideoDataOutput alloc]init];
    captureOutput.alwaysDiscardsLateVideoFrames = YES;
    _captureSession.sessionPreset = AVCaptureSessionPreset640x480;
    
    
    dispatch_queue_t queue;
    queue = dispatch_queue_create("cameraQueue", NULL);
    [captureOutput setSampleBufferDelegate:self queue:queue];
    NSString* key = (NSString*)kCVPixelBufferPixelFormatTypeKey;
    NSNumber* value = [NSNumber numberWithUnsignedInt:kCVPixelFormatType_32BGRA];
    NSDictionary* videoSettings = [NSDictionary dictionaryWithObject:value forKey:key];
    [captureOutput setVideoSettings:videoSettings];
    for(AVCaptureConnection *c in captureOutput.connections)
    {
        c.videoOrientation = AVCaptureVideoOrientationLandscapeRight;
    }
    //_captureLayer.connection.videoOrientation = AVCaptureVideoOrientationPortraitUpsideDown;
    //_captureLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    
    [_captureSession addInput:captureInput];
    [_captureSession addOutput:captureOutput];
    [_captureSession commitConfiguration];
    

    //备选方案
//    //captureLayer作为显示层
//    _captureLayer = [AVCaptureVideoPreviewLayer layerWithSession: _captureSession];
//    //_captureLayer = [AVCaptureVideoPreviewLayer layerWithSessionWithNoConnection:_captureSession];
//    //_captureLayer.frame = CGRectMake(10, START_POSITION+10, WIDTH-20, 200);
//    _captureLayer.frame = self.view.frame;
//    _captureLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
//    _captureLayer.connection.videoOrientation = AVCaptureVideoOrientationLandscapeRight;
//    [self.view.layer addSublayer: _captureLayer];
    
    //UIImage作为显示层
    //_outputImageView = [[UIImageView alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(_captureLayer.frame)+10, CGRectGetWidth(_captureLayer.frame), CGRectGetHeight(_captureLayer.frame))];
    _outputImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    [self.view addSubview:_outputImageView];
    _outputImageView.contentMode = UIViewContentModeScaleAspectFit;
    _outputImageView.hidden = NO;
    
    [self.faceDisplayLayer setWithParentLayer:self.view];
    [self.view.layer addSublayer:self.faceDisplayLayer];
    //[self.faceDisplayLayer setBackgroundColor:[UIColor redColor].CGColor];
    
    /*
    UIButton *stopButton = [[UIButton alloc]initWithFrame:CGRectMake(10, CGRectGetMaxY(_outputImageView.frame)+30, CGRectGetWidth(_outputImageView.frame)/2-10, 50)];
    stopButton.backgroundColor = [UIColor blackColor];
    [stopButton setTitle:@"stop" forState:UIControlStateNormal];
    [stopButton setTitle:@"stop" forState:UIControlStateHighlighted];
    [stopButton addTarget:self action:@selector(stop) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:stopButton];
    
    UIButton *startButton = [[UIButton alloc]initWithFrame:CGRectMake(10+CGRectGetWidth(_outputImageView.frame)/2, CGRectGetMaxY(_outputImageView.frame)+30, CGRectGetWidth(_outputImageView.frame)/2-10, 50)];
    startButton.backgroundColor = [UIColor blackColor];
    [startButton setTitle:@"start" forState:UIControlStateNormal];
    [startButton setTitle:@"start" forState:UIControlStateHighlighted];
    [startButton addTarget:self action:@selector(start) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:startButton];*/
    
    UIButton *stopButton = [[UIButton alloc]initWithFrame:CGRectMake(20, HEIGHT - 90, 70, 70)];
    stopButton.backgroundColor = [UIColor orangeColor];
    stopButton.alpha = 0.5f;
    [stopButton setTitle:@"stop" forState:UIControlStateNormal];
    [stopButton setTitle:@"stop" forState:UIControlStateHighlighted];
    [stopButton addTarget:self action:@selector(stop) forControlEvents:UIControlEventTouchUpInside];
    stopButton.layer.cornerRadius = stopButton.frame.size.height / 2;
    [self.view addSubview:stopButton];
    
    UIButton *exitButton = [[UIButton alloc]initWithFrame:CGRectMake(WIDTH / 2 - 35, HEIGHT - 90, 70, 70)];
    exitButton.backgroundColor = [UIColor redColor];
    [exitButton setTitle:@"exit" forState:UIControlStateNormal];
    [exitButton setTitle:@"exit" forState:UIControlStateHighlighted];
    exitButton.alpha = 0.5f;
    [exitButton addTarget:self action:@selector(exit) forControlEvents:UIControlEventTouchUpInside];
    exitButton.layer.cornerRadius = stopButton.frame.size.height / 2;
    [self.view addSubview:exitButton];
    
    UIButton *startButton = [[UIButton alloc]initWithFrame:CGRectMake(WIDTH - 90 , HEIGHT - 90, 70, 70)];
    startButton.backgroundColor = [UIColor colorWithRed:18.0/255 green:183.0/255 blue:245.0/255 alpha:0.5];
    startButton.alpha = 0.6f;
    [startButton setTitle:@"start" forState:UIControlStateNormal];
    [startButton setTitle:@"start" forState:UIControlStateHighlighted];
    [startButton addTarget:self action:@selector(start) forControlEvents:UIControlEventTouchUpInside];
    startButton.layer.cornerRadius = startButton.frame.size.height / 2;
    [self.view addSubview:startButton];
    
    self.detector_queue = dispatch_queue_create("detector_q", 0);
    self.isDetectingFace = NO;
    self.isUsingFrontFacingCamera = NO;
    self.nameOfFacetokenMapping = [GlobalFunctions getNameOfFacetokenMapping];
    self.recognition_queue = dispatch_queue_create("recognition_queue", 0);
    _facePreview = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 100, 100)];
    _facePreview.contentMode = UIViewContentModeScaleAspectFit;
    [_outputImageView addSubview:_facePreview];
    
    self.nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, self.facePreview.frame.size.height, self.facePreview.frame.size.width, 30)];
    [_outputImageView addSubview:self.nameLabel];
}

- (UIImage *)imageFromeSampleBuffer:(CMSampleBufferRef)sampleBuffer
{
    CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CVPixelBufferLockBaseAddress(imageBuffer,0);
    uint8_t *baseAddress = (uint8_t *)CVPixelBufferGetBaseAddress(imageBuffer);
    size_t bytesPerRow = CVPixelBufferGetBytesPerRow(imageBuffer);
    size_t width = CVPixelBufferGetWidth(imageBuffer);
    size_t height = CVPixelBufferGetHeight(imageBuffer);
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(baseAddress,width, height, 8, bytesPerRow, colorSpace,kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    CGImageRef quarztImage = CGBitmapContextCreateImage(context);
    CVPixelBufferUnlockBaseAddress(imageBuffer, 0);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    UIImage *image = [UIImage imageWithCGImage:quarztImage];
    CGImageRelease(quarztImage);
    return image;
}

-(CIImage *)ciimageFromSampleBuffer:(CMSampleBufferRef)sampleBuffer
{
    CVPixelBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    CFDictionaryRef attachments = CMCopyDictionaryOfAttachments(kCFAllocatorDefault, sampleBuffer, kCMAttachmentMode_ShouldPropagate);
    CIImage *ciImage = [[CIImage alloc] initWithCVPixelBuffer:pixelBuffer options:(__bridge NSDictionary *)attachments];
    if (attachments)
        CFRelease(attachments);
    return ciImage;
}

-(NSDictionary *)imageOptionsForFaceDetector
{
    UIDeviceOrientation curDeviceOrientation = [[UIDevice currentDevice] orientation];
    int exifOrientation;
    
    /* kCGImagePropertyOrientation values
     The intended display orientation of the image. If present, this key is a CFNumber value with the same value as defined
     by the TIFF and EXIF specifications -- see enumeration of integer constants.
     The value specified where the origin (0,0) of the image is located. If not present, a value of 1 is assumed.
     
     used when calling featuresInImage: options: The value for this key is an integer NSNumber from 1..8 as found in kCGImagePropertyOrientation.
     If present, the detection will be done based on that orientation but the coordinates in the returned features will still be based on those of the image. */
    
    enum {
        PHOTOS_EXIF_0ROW_TOP_0COL_LEFT			= 1, //   1  =  0th row is at the top, and 0th column is on the left (THE DEFAULT).
        PHOTOS_EXIF_0ROW_TOP_0COL_RIGHT			= 2, //   2  =  0th row is at the top, and 0th column is on the right.
        PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT      = 3, //   3  =  0th row is at the bottom, and 0th column is on the right.
        PHOTOS_EXIF_0ROW_BOTTOM_0COL_LEFT       = 4, //   4  =  0th row is at the bottom, and 0th column is on the left.
        PHOTOS_EXIF_0ROW_LEFT_0COL_TOP          = 5, //   5  =  0th row is on the left, and 0th column is the top.
        PHOTOS_EXIF_0ROW_RIGHT_0COL_TOP         = 6, //   6  =  0th row is on the right, and 0th column is the top.
        PHOTOS_EXIF_0ROW_RIGHT_0COL_BOTTOM      = 7, //   7  =  0th row is on the right, and 0th column is the bottom.
        PHOTOS_EXIF_0ROW_LEFT_0COL_BOTTOM       = 8  //   8  =  0th row is on the left, and 0th column is the bottom.
    };
    //NSLog(@"current Orientation %ld", (long)curDeviceOrientation);
    switch (curDeviceOrientation) {
        case UIDeviceOrientationPortraitUpsideDown:  // Device oriented vertically, home button on the top
            exifOrientation = PHOTOS_EXIF_0ROW_LEFT_0COL_BOTTOM;
            break;
        case UIDeviceOrientationLandscapeLeft:       // Device oriented horizontally, home button on the right
            if (self.isUsingFrontFacingCamera)
                exifOrientation = PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT;
            else
                exifOrientation = PHOTOS_EXIF_0ROW_TOP_0COL_LEFT;
            break;
        case UIDeviceOrientationLandscapeRight:      // Device oriented horizontally, home button on the left
            if (self.isUsingFrontFacingCamera)
                exifOrientation = PHOTOS_EXIF_0ROW_TOP_0COL_LEFT;
            else
                exifOrientation = PHOTOS_EXIF_0ROW_BOTTOM_0COL_RIGHT;
            break;
        case UIDeviceOrientationPortrait:            // Device oriented vertically, home button on the bottom
        default:
            exifOrientation = PHOTOS_EXIF_0ROW_RIGHT_0COL_TOP;
            break;
    }
    
    NSDictionary *imageOptions = [NSDictionary dictionaryWithObject:[NSNumber numberWithInt:exifOrientation] forKey:CIDetectorImageOrientation];
    return imageOptions;
}

- (UIImage *)image:(UIImage *)image rotation:(UIImageOrientation)orientation
{
    long double rotate = 0.0;
    CGRect rect;
    float translateX = 0;
    float translateY = 0;
    float scaleX = 1.0;
    float scaleY = 1.0;
    
    switch (orientation) {
        case UIImageOrientationLeft:
            rotate = M_PI_2;
            rect = CGRectMake(0, 0, image.size.height, image.size.width);
            translateX = 0;
            translateY = -rect.size.width;
            scaleY = rect.size.width/rect.size.height;
            scaleX = rect.size.height/rect.size.width;
            break;
        case UIImageOrientationRight:
            rotate = 3 * M_PI_2;
            rect = CGRectMake(0, 0, image.size.height, image.size.width);
            translateX = -rect.size.height;
            translateY = 0;
            scaleY = rect.size.width/rect.size.height;
            scaleX = rect.size.height/rect.size.width;
            break;
        case UIImageOrientationDown:
            rotate = M_PI;
            rect = CGRectMake(0, 0, image.size.width, image.size.height);
            translateX = -rect.size.width;
            translateY = -rect.size.height;
            break;
        default:
            rotate = 0.0;
            rect = CGRectMake(0, 0, image.size.width, image.size.height);
            translateX = 0;
            translateY = 0;
            break;
    }
    
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    //做CTM变换
    CGContextTranslateCTM(context, 0.0, rect.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextRotateCTM(context, rotate);
    CGContextTranslateCTM(context, translateX, translateY);
    
    CGContextScaleCTM(context, scaleX, scaleY);
    //绘制图片
    CGContextDrawImage(context, CGRectMake(0, 0, rect.size.width, rect.size.height), image.CGImage);
    
    UIImage *newPic = UIGraphicsGetImageFromCurrentImageContext();
    
    return newPic;
}

#pragma mark AVCaptureSession delegate
- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection{
    if(CURRENT_FACE_DETECTOR == OPENCV)
    {
        UIImage *image = [self imageFromeSampleBuffer:sampleBuffer];
        dispatch_async(dispatch_get_main_queue(), ^{
            _outputImageView.image =  image;
        });
        if(!self.isDetectingFace)
        {//用Opencv检测面部时, 用时较长,因此将检测放到异步队列中进行, 用isDetectingFace(原子属性，互斥访问)保证一次只有一帧被检测,其他帧被抛弃保证效率
            dispatch_async(self.detector_queue, ^{
                self.isDetectingFace = YES;
                cv::Mat imageMat;
                UIImageToMat(image, imageMat);
                cv::Mat grayImage;
                cv::cvtColor(imageMat, grayImage, CV_RGBA2GRAY);
                std::vector< cv::Rect > faces;
                cv::CascadeClassifier faceDetector;
                self.cvFaceDetector.detectMultiScale(grayImage, faces);
                NSMutableArray *facesArray = [[NSMutableArray alloc]init];
                for (int i = 0; i < faces.size(); i++) {
                    cv::Rect face_i = faces[i];
                    CGRect rect = CGRectMake(faces[i].x, faces[i].y, faces[i].size().width, faces[i].size().height);
                    rect = [self resizeToDisplayLayer:rect forImage:image];
                    [facesArray addObject:[NSValue valueWithCGRect:rect]];
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.faceDisplayLayer setFaces:facesArray];
                });
                faces.clear();
                imageMat.release();
                grayImage.release();
                self.isDetectingFace = NO;
            });
        }
    }
    else
    {
        //1.从视频流的缓冲区抽取一帧图像进行处理
        UIImage *image = [self imageFromeSampleBuffer:sampleBuffer];
        //CIDetector使用ciimage
        //CIImage *ciimage = [self ciimageFromSampleBuffer:sampleBuffer];
        cv::Mat imageMat;
        UIImageToMat(image, imageMat, YES);
        
        //2.对该帧图像进行监测
        //实测中，直接在该队列检测还要快些，并不会丢帧
        //1)面部识别
        //NSArray *features = [self.faceDetector featuresInImage:ciimage options:[self imageOptionsForFaceDetector]];
        NSArray *features = [self.customFaceDetector detectFacesInImage:image];
        //2)目标跟踪
        if([features count])
        {
             for(NSValue *f in features)
             {
                 CGRect tempFaceRect = [f CGRectValue];
                 //捕获人脸，进行识别
                 if([self.multiFaceTracker isNewFace:tempFaceRect])
                 {
                     NSLog(@"got new face");
                     if(self.faceRecognitionTool == USING_FACE_PLUS_PLUS)
                     {
                         [self.multiFaceTracker addTrackingObject:tempFaceRect InFrame:image];
                         [self proccessFace:[image cutImageatRect:[self scale:tempFaceRect to:1.5]]];
                     }
                     if(self.faceRecognitionTool == USING_FACENET)
                     {
                         //弹窗处理
                         [self proccessFace:[image cutImageatRect:tempFaceRect]];
                         
                         /*
                         //opencv绘图处理
                         NSString *name = [self.faceRecognizer findFaceImage:[image cutImageatRect:tempFaceRect] InDataSet:self.vectorDic];
                         //1)从iOS颜色空间转换到Opencv颜色空间，方便使用Opencv函数绘图
                         cv::cvtColor(imageMat, imageMat, CV_RGB2BGR);
                         if(name != nil)
                         {
                             cv::putText(imageMat, cv::String([name UTF8String]), cv::Point(tempFaceRect.origin.x, tempFaceRect.origin.y), CV_FONT_HERSHEY_COMPLEX, 1, cv::Scalar(0, 255, 0));
                         }
                         else
                         {
                             cv::putText(imageMat, cv::String("陌生人"), cv::Point(tempFaceRect.origin.x, tempFaceRect.origin.y), CV_FONT_HERSHEY_COMPLEX, 1, cv::Scalar(0, 255, 0));
                         }
                         //4)又从Opencv颜色空间换回iOS颜色空间
                         cv::cvtColor(imageMat, imageMat, CV_BGR2RGB);
                          */
                     }
                 }
             
             }
            [self.multiFaceTracker updateFrame:image];
        }
        else if([self.multiFaceTracker isTracking])
        {
            [self.multiFaceTracker updateFrame:image];
        }
        
        //3.将监测结果绘制到该帧中
        if([features count] || [self.multiFaceTracker isTracking])
        {
            //1)从iOS颜色空间转换到Opencv颜色空间，方便使用Opencv函数绘图
            cv::cvtColor(imageMat, imageMat, CV_RGB2BGR);
            //2)面部监测框绘制
            if([features count])
            {
                for(NSValue *faceFeature in features)
                {
                    CGRect rect = [faceFeature CGRectValue];
                    cv::Rect cvRect = cv::Rect(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
                    cv::rectangle(imageMat, cvRect, cv::Scalar(0, 255, 0), 4);
                    //NSLog(@"call draw face");
                    //已经有object被跟踪时，需要检测是否为新面孔
                    CGRect newRect = [self scale:rect to:1.5];
                    cv::Rect newcvRect = cv::Rect(newRect.origin.x, newRect.origin.y, newRect.size.width, newRect.size.height);
                    cv::rectangle(imageMat, newcvRect, cv::Scalar(255, 255, 0), 4);
                }
            }
            //3)追踪层绘制
            NSArray *trackingObjectRects = [self.multiFaceTracker trackingObejctRects];
            for(NSValue *rectValue in trackingObjectRects)
            {
                CGRect trackingRect = [rectValue CGRectValue];
                cv::Rect cvTrackingRect = cv::Rect(trackingRect.origin.x, trackingRect.origin.y, trackingRect.size.width, trackingRect.size.height);
                cv::rectangle(imageMat, cvTrackingRect, cv::Scalar(0, 0, 255), 4);
            }
            //4)又从Opencv颜色空间换回iOS颜色空间
            cv::cvtColor(imageMat, imageMat, CV_BGR2RGB);
        }
        //4.输出至屏幕
        UIImage *displayImage = MatToUIImage(imageMat);
        if(self.isUsingFrontFacingCamera)
        {
            displayImage = [UIImage imageWithCGImage:displayImage.CGImage scale:1.0 orientation:UIImageOrientationDownMirrored];;
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [_outputImageView setImage:displayImage];
        });
    }
}


-(cv::Mat)upsideDown:(cv::Mat &)srcImage
{
    cv::Mat dstImage(srcImage.size(), srcImage.type());
    //定义x和y方向的矩阵
    cv::Mat xMap(srcImage.size(), CV_32FC1);
    cv::Mat yMap(srcImage.size(), CV_32FC1);
    //获取图像的宽和高
    int rowNumber = srcImage.rows;
    int colNumber = srcImage.cols;
    
    //对图像进行遍历操作
    for (int i = 0; i < rowNumber; i++)
    {
        for (int j = 0; j < colNumber; j++)
        {
            //x和y都进行翻转操作
            xMap.at<float>(i, j) = static_cast<float>(srcImage.cols - j);
            yMap.at<float>(i, j) = static_cast<float>(srcImage.rows - i);
        }
    }
    
    //进行重映射操作
    remap(srcImage, dstImage, xMap, yMap,
          CV_INTER_LINEAR, cv::BORDER_CONSTANT, cv::Scalar(0, 0, 0));
    return dstImage;
}

//从ciimage的坐标系变换到UIImage的坐标系
-(CGRect)changeCoordinate:(CGRect)rect forImage:(UIImage *)image
{
    CGRect newRect = CGRectMake(rect.origin.x,
                                image.size.height - rect.origin.y - rect.size.height,
                                rect.size.width,
                                rect.size.height);
    return newRect;
}

//原始图像的坐标系变换到屏幕坐标系(原始图像过大过小, 比例和屏幕比例不等时会有放缩, 坐标需要做相应变幻)
-(CGRect)resizeToDisplayLayer:(CGRect)rect forImage:(UIImage *)image
{
    CGRect newRect;
    if(currentDisplayMode == PreViewLayerMode)
    {
        //允许图像压缩时使用frame代替
        CGFloat widthScaleBy = _captureLayer.frame.size.width / image.size.width;
        CGFloat heightScaleBy = _captureLayer.frame.size.height / image.size.height;
        rect.size.width *= widthScaleBy;
        rect.size.height *= heightScaleBy;
        rect.origin.x *= widthScaleBy;
        //if(_captureLayer.videoGravity == AVLayerVideoGravityResizeAspectFill) rect.origin.x -= WIDTH * (widthScaleBy - 1);
        rect.origin.y *= heightScaleBy;
        newRect = rect;
    }
    else
    {
        CGFloat widthScaleBy = _outputImageView.image.size.width / image.size.width;
        CGFloat heightScaleBy = _outputImageView.image.size.height / image.size.height;
        rect.size.width *= widthScaleBy;
        rect.size.height *= heightScaleBy;
        rect.origin.x *= widthScaleBy;
        rect.origin.y *= heightScaleBy;
        newRect = rect;
    }
    return newRect;
}

-(NSArray *)getFaceRectArrayFromFeatures:(NSArray *)features for:(UIImage *)image
{
    NSMutableArray *faceArray = [[NSMutableArray alloc]init];
    for ( CIFaceFeature *ff in features ) {
        CGRect faceRect = [ff bounds];
        // flip preview width and height
        //CGFloat temp = faceRect.size.width;
        //faceRect.size.width = faceRect.size.height;
        //faceRect.size.height = temp;
        //temp = faceRect.origin.x;
        //faceRect.origin.x = faceRect.origin.y;
        //faceRect.origin.y = temp;
        faceRect = [self changeCoordinate:faceRect forImage:image];
        faceRect =  [self resizeToDisplayLayer:faceRect forImage:image];
        [faceArray addObject:[NSValue valueWithCGRect:faceRect]];
    }
    return [NSArray arrayWithArray:faceArray];
}


-(void)exit
{
    NSDate *date = [NSDate date];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate: date];
    NSDate *localeDate = [date  dateByAddingTimeInterval: interval];
    _record.startTime = localeDate;
    self.record.endTime = localeDate;
    
    [self.table addLinesObject:self.record];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:STUDENT_ENTITYNAME];
    NSArray *students = [self.context executeFetchRequest:request error:nil];
    self.record.targetAttendance = (int32_t)[students count];
    
    NSLog(@"table when exit:%@", self.table);
    NSLog(@"record when exit:%@", self.record);
    @try {
        [self.context save:nil];
        NSLog(@"context saved");
    } @catch (NSException *exception) {
        [self.context rollback];
        dispatch_async(dispatch_get_main_queue(), ^{
            [GlobalFunctions showSuccessAlertTo:self withTitle:exception.name Message:exception.description buttonName:@"OK"];
        });
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)resetDetectorAndTracker
{
    self.multiFaceTracker = nil;
    self.faceDetector = nil;
}

-(void)stop{
    //[self switchCameras];
    
    [_captureSession stopRunning];
    dispatch_async(self.detector_queue, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            _outputImageView.image = nil;
        });
        //[self.faceTracker stopTracker];
        self.multiFaceTracker = nil;
    });
    
}
-(void)start{
    [_captureSession startRunning];
}

- (void)switchCameras
{
    AVCaptureDevicePosition desiredPosition;
    if (self.isUsingFrontFacingCamera)
        desiredPosition = AVCaptureDevicePositionBack;
    else
        desiredPosition = AVCaptureDevicePositionFront;
    
    for (AVCaptureDevice *d in self.videoDeviceDiscoverySession.devices) {
        if ([d position] == desiredPosition) {
            [_captureSession beginConfiguration];
            AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:d error:nil];
            for (AVCaptureInput *oldInput in [_captureSession inputs]) {
                [_captureSession removeInput:oldInput];
            }
            [_captureSession addInput:input];
            [self resetDetectorAndTracker];
            [_captureSession commitConfiguration];
            break;
        }
    }
    self.isUsingFrontFacingCamera = !self.isUsingFrontFacingCamera;
}


#pragma mark - 面部识别

-(void)proccessFace:(UIImage *)faceImage
{
    //1.resize
    if(self.isUsingFrontFacingCamera && self.faceRecognitionTool == USING_FACE_PLUS_PLUS)
    {
        faceImage = [UIImage imageWithCGImage:faceImage.CGImage scale:1.0 orientation:UIImageOrientationDownMirrored];
        if(faceImage.size.width < 150)
        {
            faceImage = [OpencvTools reszie:faceImage To:CGSizeMake(300, 300)];
        }
    }
    //if(self.faceRecognitionTool == USING_FACENET) faceImage = [OpencvTools reszie:faceImage To:CGSizeMake(96, 96)];
    //2.display sample image
    dispatch_async(dispatch_get_main_queue(), ^{
        _facePreview.image = faceImage;
    });
    //3.recognition
    if(self.faceRecognitionTool == USING_FACE_PLUS_PLUS) [self.facePlusPlusTool searchFaceInFaceSet:faceplusplus_api_faceset_token usingFaceImage:faceImage];
    if(self.faceRecognitionTool == USING_FACENET)
    {
        dispatch_async(self.recognition_queue, ^{
            NSString *name = [self.faceRecognizer findFaceImage:faceImage InDataSet:self.vectorDic];
            dispatch_async(dispatch_get_main_queue(), ^{
                if(name != nil)
                {
                    self.nameLabel.text = name;
                    [GlobalFunctions showSuccessAlertTo:self withTitle:name Message:@"识别成功" buttonName:@"OK" duration:1];
                }
                else
                {
                    self.nameLabel.text = @"陌生人";
                }
            });
        });
    }
}

//callback
-(void)finishFaceRecognition:(NSData *)data
{
    NSDictionary *obj = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSString *face_token = [NSString stringWithFormat:@"%@", [obj valueForKeyPath:@"results.face_token"]];
    face_token = [GlobalFunctions removeSpareCharIn:face_token];
    NSString *confidence = [NSString stringWithFormat:@"%@", [obj valueForKeyPath:@"results.confidence"]];
    confidence = [GlobalFunctions removeSpareCharIn:confidence];
    confidence = [confidence stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    NSString *name = [self.nameOfFacetokenMapping valueForKey:face_token];
    double confidenceFloat = [confidence doubleValue];
    NSLog(@"recived data:%@", obj);
    NSLog(@"string value:%@", confidence);
    NSLog(@"final result: (%@, %lf)", face_token, confidenceFloat);
    
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:STUDENT_ENTITYNAME];
    request.predicate = [NSPredicate predicateWithFormat:@"face_token = %@", face_token];
    NSArray *students = [self.context executeFetchRequest:request error:nil];
    Student *student = [students firstObject];
    if(student)
    {
        [self.record addAttendanceStudentsObject:student];
        [student addRecordsObject:self.record];
        [self.context save:nil];
    }
    NSLog(@"%@", student);
    NSLog(@"%@", self.record);
    dispatch_async(dispatch_get_main_queue(), ^{
        if([name length] && confidenceFloat > face_recognition_confidence_threshold)
        {
            self.nameLabel.text = name;
            [GlobalFunctions showSuccessAlertTo:self withTitle:name Message:@"识别成功" buttonName:@"OK" duration:1];
        }
        else if(!name || confidenceFloat <= face_recognition_confidence_threshold)
        {
            self.nameLabel.text = @"陌生人";
        }
    });
}

-(CGRect)scale:(CGRect)rect to:(double)time
{
    CGRect newRect = rect;
    newRect.size.width *= time;
    newRect.size.height *= time;
    newRect = CGRectOffset(newRect, rect.size.width * (1 - time) / 2, rect.size.height * (1 - time) / 2);
    return newRect;
}

-(void)dismissAlet
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
