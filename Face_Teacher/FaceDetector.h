//
//  FaceDetector.h
//  Face_Teacher
//
//  Created by jackstevenson on 17/4/28.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreVideo/CoreVideo.h>
#import <CoreImage/CoreImage.h>
#import <ImageIO/ImageIO.h>

@interface FaceDetector : NSObject
-(UIImage *)getBigestFcaeInImage:(UIImage *)image;
-(UIImage *)cvGetBigestFaceInImage:(UIImage *)image;
-(NSArray *)detectFacesInImage:(UIImage *)image;
@end
