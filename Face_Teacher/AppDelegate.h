//
//  AppDelegate.h
//  Face_Teacher
//
//  Created by jackstevenson on 17/3/17.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "GlobalSetting.h"
#import "Student+CoreDataClass.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>
typedef NS_ENUM(NSInteger, ObejctTypeOfMyCoreData){
    TeachingClassType,
    StudentType,
    TeacherType,
    CourseType
};
@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;
- (BOOL)saveContextWithReturnValue:(NSError **)error;
@end

