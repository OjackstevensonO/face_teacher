//
//  GlobalFunctions.h
//  Face_Teacher
//
//  Created by jackstevenson on 17/3/18.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SCLAlertView.h"
#import "GlobalSetting.h"
#import "TeachingClass+CoreDataClass.h"
#import "Student+CoreDataClass.h"
typedef void (^actionBlock)(void);
@interface GlobalFunctions : NSObject
+(void)showErrorAlertTo:(UIViewController *)controller withTitle:(NSString *)title Message:(NSString *)message buttonName:(NSString *)buttonName;
+(void)showSuccessAlertTo:(UIViewController *)controller withTitle:(NSString *)title Message:(NSString *)message buttonName:(NSString *)buttonName;
+(BOOL)value:(NSString *)value ofKey:(NSString *)keyName isExistInEntity:(NSString *)entityName Error:(NSError **)error;
+(void)deletStudentsImagesIn:(TeachingClass *)teachingClass;
+(BOOL)isIntersect:(CGRect)rectA andRectB:(CGRect)rectB;
+(NSString *)removeSpareCharIn:(NSString *)string;
+(NSDictionary *)getNameOfFacetokenMapping;
+(void)showSuccessAlertTo:(UIViewController *)controller withTitle:(NSString *)title Message:(NSString *)message buttonName:(NSString *)buttonName duration:(NSTimeInterval)time;
+(void)showErrorAlertTo:(UIViewController *)controller withTitle:(NSString *)title Message:(NSString *)message buttonName:(NSString *)buttonName confirm:(SCLActionBlock)confirmActionBlock withCancelButton:(NSString *)cancelButtonName canceled:(SCLActionBlock)cancelBlock;
@end
