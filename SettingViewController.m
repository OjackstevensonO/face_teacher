//
//  SettingViewController.m
//  Face_Teacher
//
//  Created by jackstevenson on 17/5/10.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import "SettingViewController.h"
#import "FacePlusPlusTools.h"
#import "GlobalFunctions.h"
#import "Record+CoreDataClass.h"
#import "RecordTable+CoreDataClass.h"
#import "AppDelegate.h"
#import "GlobalSetting.h"

@interface SettingViewController ()<FacePlusPlusDelegate>
@property (strong, nonatomic) FacePlusPlusTools *tool;
@property (strong, nonatomic) NSManagedObjectContext *context;
@property (strong, nonatomic) AppDelegate *appDelegate;
@property (weak, nonatomic) IBOutlet UISwitch *faceRecognitionScheme;
@end

@implementation SettingViewController

- (AppDelegate *)appDelegate
{
    if(!_appDelegate)
    {
        _appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    return _appDelegate;
}

- (NSManagedObjectContext *)conext
{
    if(!_context)
    {
        _context =  [self.appDelegate persistentContainer].viewContext;
    }
    return _context;
}
- (IBAction)changeFaceRecognitionScheme:(UISwitch *)sender
{
    //default == False
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    //保存
    [defaults setObject:[NSNumber numberWithBool:sender.isOn] forKey:FACE_RECOGNITION_SCHEME];
    //及时同步
    [defaults synchronize];
}

- (IBAction)resetAttendanceData:(UIButton *)sender
{
    //NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:RECORD_TABLE_ENTITYNAME];
    if(!self.context) NSLog(@"context == nil");
    @try {
//        NSArray *tables = [self.context executeFetchRequest:request error:nil];
//        for(RecordTable *r in tables)
//        {
//            [self.context deleteObject:r];
//        }
//        NSError *error1;
//        [self.context save:&error1];
//        NSLog(@"error1:%@", error1);
        
        NSError *error2;
        RecordTable *newTable = [NSEntityDescription insertNewObjectForEntityForName:@"RecordTable" inManagedObjectContext:self.context];
        newTable.name = @"log";
        [self.appDelegate saveContext];
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"RecordTable"];
        NSArray *newtables = [self.context executeFetchRequest:request error:nil];
        NSLog(@"newtable:%@", newtables);
        NSLog(@"error2:%@", error2);
    } @catch (NSException *exception) {
        [self.context rollback];
        dispatch_async(dispatch_get_main_queue(), ^{
            [GlobalFunctions showSuccessAlertTo:self withTitle:exception.name Message:exception.description buttonName:@"OK"];
        });
    }
}

-(FacePlusPlusTools *)tool
{
    if(!_tool) _tool = [[FacePlusPlusTools alloc]init];
    return _tool;
}

-(void)removeFaceSuccess:(NSData *)data
{
    id obj = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    NSLog(@"%@",[obj class]);
    NSLog(@"%@",obj);
    
    NSString *removedFaceCount = [NSString stringWithFormat:@"%@", [obj valueForKeyPath:@"face_removed"]];
    dispatch_async(dispatch_get_main_queue(), ^{
        [GlobalFunctions showSuccessAlertTo:self withTitle:@"success" Message:[NSString stringWithFormat:@"removed %@ face", removedFaceCount] buttonName:@"OK"];
    });
}

-(void)dealWithError:(NSData *)data response:(NSURLResponse *)response andError:(NSError *)error
{
    NSLog(@"error, data:%@ response:%@ error:%@", data, response, error);
}

- (FaceRecognitionTool)getCurFaceRecognitionScheme
{
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    //读取
    NSNumber *value = [defaults objectForKey:FACE_RECOGNITION_SCHEME];
    if(value == nil)
    {
        [defaults setObject:[NSNumber numberWithInt:USING_FACE_PLUS_PLUS] forKey:FACE_RECOGNITION_SCHEME];
        [defaults synchronize];
        return USING_FACE_PLUS_PLUS;
    }
    int n_scheme = [value intValue];
    switch (n_scheme) {
        case USING_FACE_PLUS_PLUS:
            return USING_FACE_PLUS_PLUS;
            break;
        case USING_FACENET:
            return USING_FACENET;
            break;
        default:
            return USING_FACE_PLUS_PLUS;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tool.delegate = self;
    [self.faceRecognitionScheme setOn:(int)[self getCurFaceRecognitionScheme] == 0 ? false : true];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
