//
//  FaceDisplayLayer.m
//  Face_Teacher
//
//  Created by jackstevenson on 17/4/19.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import "FaceDisplayLayer.h"

@interface FaceDisplayLayer()
@property (nonatomic) BOOL hasFaceRectOnLayer;
@property (nonatomic) BOOL shouldDisplay;
@end

@implementation FaceDisplayLayer

-(void)setFaces:(NSArray *)faces
{
    _faces = faces;
    if(!self.shouldDisplay)
    {
        [self setNeedsDisplay];
    }
}

-(void)setTrackerBox:(CGRect)trackerBox
{
    _trackerBox = trackerBox;
    if(!self.shouldDisplay)
    {
        [self setNeedsDisplay];
    }
}

- (void)drawInContext:(CGContextRef)ctx
{
    
    CGContextSetRGBStrokeColor(ctx, 0.0f, 1.0f, 0.0f, 1);
    CGContextSetLineWidth(ctx, 4);
    for(NSValue *faceRectValue in self.faces)
    {
        CGRect faceRect = [faceRectValue CGRectValue];
        CGContextAddRect(ctx, faceRect);
    }
    CGContextDrawPath(ctx, kCGPathStroke);
    //NSLog(@"draw faceRec");
    
    CGContextSetRGBStrokeColor(ctx, 1.0f, 0.0f, 0.0f, 1);
    //CGContextSetLineWidth(ctx, 4);
    CGContextAddRect(ctx, self.trackerBox);
    CGContextDrawPath(ctx, kCGPathStroke);
    /*NSLog(@"tracker box:(%f, %f, %f, %f)", self.trackerBox.origin.x, self.trackerBox.origin.y,
          self.trackerBox.size.width, self.trackerBox.size.height);
     */
    
    //NSLog(@"draw trackerRect");
    self.shouldDisplay = NO;
}

-(instancetype)init
{
    self = [super init];
    if(self)
    {
        self.shouldDisplay = NO;
    }
    return self;
}

- (void)setWithParentLayer:(UIView *)parentView
{
    self.bounds = parentView.bounds;
    self.frame = parentView.frame;
}
@end
