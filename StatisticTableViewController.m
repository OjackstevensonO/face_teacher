//
//  StatisticTableViewController.m
//  Face_Teacher
//
//  Created by jackstevenson on 17/6/9.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import "StatisticTableViewController.h"
#import "Record+CoreDataClass.h"
#import "AppDelegate.h"
#import "RecordTable+CoreDataClass.h"
#import "AttendanceTableViewController.h"

@interface StatisticTableViewController ()
@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic,strong) NSArray *myRecordArray;
@end

@implementation StatisticTableViewController


- (NSArray *)myRecordArray
{
    if(!_myRecordArray)
    {
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Record"];
        request.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"startTime" ascending:NO]];
        _myRecordArray = [self.context executeFetchRequest:request error:nil];
    }
    return _myRecordArray;
}


- (NSManagedObjectContext *)context
{
    if(!_context)
    {
        AppDelegate *_appDeleget = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSPersistentContainer *persistentContainer = [_appDeleget persistentContainer];
        _context =  persistentContainer.viewContext;
    }
    return _context;
}

- (void)viewWillAppear:(BOOL)animated
{
    self.myRecordArray = nil;
    [self.tableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
	
    NSLog(@"%lu",(unsigned long)self.myRecordArray.count);
    return self.myRecordArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"recordCell" forIndexPath:indexPath];
    
    // Configure the cell...
    Record *r = self.myRecordArray[indexPath.row];
    NSDate *startTime = r.startTime;
    NSDate *endTime = r.endTime;
    NSString *attendance = [NSString stringWithFormat:@"%ld", r.attendanceStudents.count];
    NSString *title = [NSString stringWithFormat:@"时间:%@ --> %@", startTime, endTime];
    cell.textLabel.text = title;
    NSString *targrtAttendace = [NSString stringWithFormat:@"%d", r.targetAttendance];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"出勤率：%@/%@", attendance, targrtAttendace];
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    AttendanceTableViewController *atvc = [segue destinationViewController];
    atvc.record = self.myRecordArray[indexPath.row];
}


@end
