//
//  OpencvTools.h
//  Face_Teacher
//
//  Created by jackstevenson on 17/4/28.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreVideo/CoreVideo.h>
#import <CoreImage/CoreImage.h>

@interface OpencvTools : NSObject
+(UIImage *)reszie:(UIImage *)image To:(CGSize)size;
+(UIImage *)getPGMImageFromPath:(NSString *)path;
@end
