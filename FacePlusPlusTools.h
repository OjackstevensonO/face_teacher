//
//  FacePlusPlusTools.h
//  Face_Teacher
//
//  Created by jackstevenson on 17/4/27.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GlobalSetting.h"
#import "FacePlusPlusDelegate.h"

@interface FacePlusPlusTools : NSObject
-(void)createOenFaceSet;
//-(void)detectFaceOnServer:(UIImage *)faceImage;
-(void)addFace:(UIImage *)faceImage ToFaceSet:(NSString *)face_set;
-(void)searchFaceInFaceSet:(NSString *)face_set usingFaceImage:(UIImage *)faceImage;
-(void)removeFace:(NSString *)face_token InFaceSet:(NSString *)face_set;
@property (nonatomic, weak) id<FacePlusPlusDelegate> delegate;
@end
