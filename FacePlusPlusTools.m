//
//  FacePlusPlusTools.m
//  Face_Teacher
//
//  Created by jackstevenson on 17/4/27.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import "GlobalFunctions.h"
#import "FacePlusPlusTools.h"
#import "OpencvTools.h"

@interface FacePlusPlusTools()

@end

@implementation FacePlusPlusTools

#pragma mark - 从Faceset中删除人脸
-(void)removeFace:(NSString *)face_token InFaceSet:(NSString *)face_set
{
    //if(!face_set) face_set = faceplusplus_api_faceset_token;
    //UIImage *tinyFaceImage = [OpencvTools reszie:faceImage To:CGSizeMake(100, 100)];
    NSMutableURLRequest *request = [self createPostRequestOfURLStr:faceplusplus_api_faceset_removeface];
    NSMutableData *bodyData = [[NSMutableData alloc]init];
    [self addApiKeyToData:bodyData];
    [self addKey:@"faceset_token" andData:faceplusplus_api_faceset_token toBody:bodyData];
    [self addKey:@"face_tokens" andData:face_token toBody:bodyData];
    [self addBoundaryToBodyData:bodyData];
    request.HTTPBody = bodyData;
    NSURLSessionTask *task = [[NSURLSession sharedSession] uploadTaskWithRequest:request fromData:bodyData completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if (error) {
            [self.delegate dealWithError:data response:response andError:error];
            
        }else{
            switch (httpResponse.statusCode) {
                case 200:
                {
                    NSDictionary *result = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    NSInteger removedCount = [[result valueForKey:@"face_removed"] integerValue];
                    NSLog(@"%@", result);
                    if(removedCount) [self.delegate removeFaceSuccess:data];
                    else [self.delegate dealWithError:data response:response andError:error];
                }
                    break;
                default:
                    [self.delegate dealWithError:data response:response andError:error];
                    break;
            }
        }
    }];
    [task resume];
}

#pragma mark - 添加人脸到FaceSet

//1.提交image到server获取该image的face_token
//2.将该face_koen添加到face_set
-(void)addFace:(UIImage *)faceImage ToFaceSet:(NSString *)face_set
{
    //if(!face_set) face_set = faceplusplus_api_faceset_token;
    if(faceImage.size.width > 300)
    {
        faceImage = [OpencvTools reszie:faceImage To:CGSizeMake(300, 300)];
    }
    [self detectFaceOnServer:faceImage];
}

-(void)detectFaceOnServer:(UIImage *)faceImage
{
    //UIImage *tinyFaceImage = [OpencvTools reszie:faceImage To:CGSizeMake(100, 100)];
    NSMutableURLRequest *request = [self createPostRequestOfURLStr:faceplusplus_api_faceset_detect_url];
    NSMutableData *bodyData = [[NSMutableData alloc]init];
    [self addApiKeyToData:bodyData];
    [self addKey:@"faceset_token" andData:faceplusplus_api_faceset_token toBody:bodyData];
    //addFace
    [self addImage:faceImage toBodyData:bodyData withName:@"image_file"];
    [self addBoundaryToBodyData:bodyData];
    request.HTTPBody = bodyData;
    NSURLSessionTask *task = [[NSURLSession sharedSession] uploadTaskWithRequest:request fromData:bodyData completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if (error) {
            NSLog(@"%@", response);
            NSLog(@"%@",error);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate dealWithError:data response:response andError:error];
            });
        }else{
            switch (httpResponse.statusCode) {
                case 200:
                {
                    id obj = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                    NSLog(@"%@",[obj class]);
                    NSLog(@"%@",obj);
                    
                    NSString *face_token = [NSString stringWithFormat:@"%@", [obj valueForKeyPath:@"faces.face_token"]];
                    face_token = [GlobalFunctions removeSpareCharIn:face_token];
                    NSLog(@"face token: %@", face_token);
                    [self addFaceTokenToFaceSet:face_token];
                }
                    break;
                default:
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.delegate dealWithError:data response:response andError:error];
                    });
                    break;
            }
        }
    }];
    [task resume];
}

-(void)addFaceTokenToFaceSet:(NSString *)face_token
{
    NSMutableURLRequest *request = [self createPostRequestOfURLStr:faceplusplus_api_faceset_addface_url];
    NSMutableData *bodyData = [[NSMutableData alloc]init];
    [self addApiKeyToData:bodyData];
    [self addKey:@"faceset_token" andData:faceplusplus_api_faceset_token toBody:bodyData];
    [self addKey:@"face_tokens" andData:face_token toBody:bodyData];
    [self addBoundaryToBodyData:bodyData];
    request.HTTPBody = bodyData;
    NSURLSessionTask *task = [[NSURLSession sharedSession] uploadTaskWithRequest:request fromData:bodyData completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if (error) {
            NSLog(@"%@", response);
            NSLog(@"%@",error);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate dealWithError:data response:response andError:error];
            });
        }else{
            switch (httpResponse.statusCode) {
                case 200:
                {
                    id obj = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                    NSLog(@"%@",[obj class]);
                    NSLog(@"%@",obj);
                    //成功
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.delegate finishAddFaceToServer:data usingFaceToken:face_token];
                    });
                }
                    break;
                default:
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.delegate dealWithError:data response:response andError:error];
                    });
                    break;
            }
        }
    }];
    [task resume];
}


#pragma mark - 人脸识别(在人脸库中查找)

-(void)searchFaceInFaceSet:(NSString *)face_set usingFaceImage:(UIImage *)faceImage
{
    ///UIImage *tinyFaceImage = [OpencvTools reszie:faceImage To:CGSizeMake(100, 100)];
    NSMutableURLRequest *request = [self createPostRequestOfURLStr:faceplusplus_api_search_face_url];
    NSMutableData *bodyData = [[NSMutableData alloc]init];
    [self addApiKeyToData:bodyData];
    [self addImage:faceImage toBodyData:bodyData withName:@"image_file"];
    [self addKey:@"faceset_token" andData:face_set toBody:bodyData];
    [self addBoundaryToBodyData:bodyData];
    request.HTTPBody = bodyData;
    NSURLSessionTask *task = [[NSURLSession sharedSession] uploadTaskWithRequest:request fromData:bodyData completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
        if (error) {
            NSLog(@"%@", response);
            NSLog(@"%@",error);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.delegate dealWithError:data response:response andError:error];
            });
        }else{
            switch (httpResponse.statusCode) {
                case 200:
                {
                    [self.delegate finishFaceRecognition:data];
                }
                    break;
                default:
                {
                    id obj = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                    NSLog(@"%@",[obj class]);
                    NSLog(@"%@",obj);
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //[self.delegate dealWithError:data response:response andError:error];
                    });
                }
                    break;
            }
        }
    }];
    [task resume];
}


/**
        display_name:String	人脸集合的名字，最长256个字符，不能包括字符^@,&=*'"
        outer_id:String	    账号下全局唯一的FaceSet自定义标识，可以用来管理FaceSet对象。最长255个字符，不能包括字符^@,&=*'"
 */

#pragma mark - 辅助函数

-(NSMutableURLRequest *)createPostRequestOfURLStr:(NSString *)urlStr
{
    NSURL *url = [NSURL URLWithString:urlStr];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",@"boundary"];
    [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
    return request;
}

-(void)createOenFaceSet
{
    NSMutableURLRequest *request = [self createPostRequestOfURLStr:faceplusplus_api_create_faceset_url];
    NSMutableData *bodyData = [[NSMutableData alloc]init];
    [self addKey:@"api_key" andData:faceplusplus_api_key toBody:bodyData];
    [self addKey:@"api_secret" andData:faceplusplus_api_secret toBody:bodyData];
    [self addKey:@"display_name" andData:@"TestSet" toBody:bodyData];
    [self addKey:@"outer_id" andData:@"FaceSet001" toBody:bodyData];
    [self addBoundaryToBodyData:bodyData];
    request.HTTPBody = bodyData;
    
    NSURLSessionTask *task = [[NSURLSession sharedSession] uploadTaskWithRequest:request fromData:bodyData completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            NSLog(@"%@", response);
            NSLog(@"%@",error);
        }else{
            id obj = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            NSLog(@"%@",[obj class]);
            NSLog(@"%@",obj);
        }
    }];
    [task resume];
}

-(void)addBoundaryToBodyData:(NSMutableData *)bodyData
{
    [bodyData appendData:[@"--boundary--\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
}


-(void)addKey:(id)key andData:(id)obj toBody:(NSMutableData *)bodyData
{
    [bodyData appendData:[@"--boundary\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [bodyData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",key] dataUsingEncoding:NSUTF8StringEncoding]];
    [bodyData appendData:[[NSString stringWithFormat:@"%@\r\n",obj] dataUsingEncoding:NSUTF8StringEncoding]];
}

-(void)addApiKeyToData:(NSMutableData *)bodyData
{
    NSDictionary *dic =@{
                         @"api_key" : faceplusplus_api_key,//你的api_key
                         @"api_secret" : faceplusplus_api_secret,//你的api_secret
                         };
    [dic enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        [bodyData appendData:[@"--boundary\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [bodyData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",key] dataUsingEncoding:NSUTF8StringEncoding]];
        [bodyData appendData:[[NSString stringWithFormat:@"%@\r\n",obj] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
}


- (void)sessionCompare{
    
    NSString *urlStr = @"https://api-cn.faceplusplus.com/facepp/v3/compare";
    NSURL *url = [NSURL URLWithString:urlStr];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",@"boundary"];
    [request setValue:contentType forHTTPHeaderField:@"Content-Type"];
    NSDictionary *dict = @{
                           @"api_key" : @"xxxxxxxxxxxxx",//你的api_key
                           @"api_secret" : @"xxxxxxxxxxxxx",//你的api_secret
                           };
    NSString *path1 = [[NSBundle mainBundle] pathForResource:@"image1" ofType:@"jpeg"];
    NSString *path2 = [[NSBundle mainBundle] pathForResource:@"image2" ofType:@"jpeg"];
    NSData *data1 = [NSData dataWithContentsOfFile:path1];
    NSData *data2 = [NSData dataWithContentsOfFile:path2];
    ///////////////////////NSURLSession 示例如下:
    NSData *bodyData = [self bodyDataWithParam:dict fileData:@[
                                                               @{@"fieldName" : @"image_file1" ,@"data" : data1 , @"fileType" : @"jpeg"},
                                                               @{@"fieldName" : @"image_file2" ,@"data" : data2 , @"fileType" : @"jpeg"}]];
    NSURLSessionTask *task = [[NSURLSession sharedSession] uploadTaskWithRequest:request fromData:bodyData completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error) {
            NSLog(@"%@",error);
        }else{
            id obj = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            NSLog(@"%@",[obj class]);
            NSLog(@"%@",obj);
        }
    }];
    [task resume];
}


-(void)addImage:(UIImage *)image toBodyData:(NSMutableData *)bodyData withName:(NSString *)filedName
{
    NSData *fileData = UIImagePNGRepresentation(image);
    NSString *fileType = @"png";
    [self addData:fileData ToBodyData:bodyData withFieldName:filedName fileType:fileType];
}


-(void)addData:(NSData *)fileData ToBodyData:(NSMutableData *)bodyData withFieldName:(NSString *)fieldName fileType:(NSString *)fileType
{
    [bodyData appendData:[@"--boundary\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [bodyData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldName, fieldName] dataUsingEncoding:NSUTF8StringEncoding]];
    [bodyData appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", fileType] dataUsingEncoding:NSUTF8StringEncoding]];
    [bodyData appendData:fileData];
    [bodyData appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
}



- (NSData *)bodyDataWithParam:(NSDictionary *)param fileData:(NSArray<NSDictionary *> *)fileDatas{
    NSMutableData *bodyData = [NSMutableData data];
    //拼接参数
    
    [param enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        [bodyData appendData:[@"--boundary\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [bodyData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",key] dataUsingEncoding:NSUTF8StringEncoding]];
        [bodyData appendData:[[NSString stringWithFormat:@"%@\r\n",obj] dataUsingEncoding:NSUTF8StringEncoding]];
    }];
    
    //拼接文件二进制数据
    for (NSDictionary *dic in fileDatas) {
        NSString *fieldName = dic[@"fieldName"];
        NSData *fileData = dic[@"data"];
        NSString *fileType = dic[@"fileType"];
        [bodyData appendData:[@"--boundary\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [bodyData appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", fieldName, fieldName] dataUsingEncoding:NSUTF8StringEncoding]];
        [bodyData appendData:[[NSString stringWithFormat:@"Content-Type: %@\r\n\r\n", fileType] dataUsingEncoding:NSUTF8StringEncoding]];
        [bodyData appendData:fileData];
        [bodyData appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    }
    [bodyData appendData:[@"--boundary--\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    return bodyData;
}

@end
