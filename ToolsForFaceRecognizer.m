//
//  ToolsForFaceRecognizer.m
//  Face_Teacher
//
//  Created by jackstevenson on 2018/4/27.
//  Copyright © 2018年 jackstevenson. All rights reserved.
//

#import "ToolsForFaceRecognizer.h"

@implementation ToolsForFaceRecognizer
-(double)around:(double)num decimals:(int)len
{
    double ans;
    ans = (long long)(num * pow(10, len) + 0.5);
    return ans / pow(10, len);
}

-(MLMultiArray *)L2_Norm:(MLMultiArray *)vector
API_AVAILABLE(ios(11.0)){
    double sum = 0, epsilon = 1e-12;//epsilon 参考tensoflow文档
    MLMultiArray *new_vector = [[MLMultiArray alloc]initWithShape:vector.shape dataType:MLMultiArrayDataTypeDouble error:nil];
    for(int j = 0; j < vector.count; j++)
    {
        sum += pow([vector[j] doubleValue], 2);
    }
    double x_norm = sqrt(MAX(sum, epsilon));
    for(int j = 0; j < vector.count; j++)
    {
        new_vector[j] = [NSNumber numberWithDouble:[vector[j] doubleValue] / x_norm];
    }
    return new_vector;
}

-(MLMultiArray *)imageToArray:(UIImage *)image
{
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = CGImageGetWidth(image.CGImage), rows = CGImageGetHeight(image.CGImage);
    CGContextRef contextRef;
    CGBitmapInfo bitmapInfo = (CGBitmapInfo)kCGImageAlphaPremultipliedLast;
    // 8 bits per component, 4 channels
    void *data = (void *)malloc(cols * rows * 4 * sizeof(unsigned char));
    contextRef = CGBitmapContextCreate(data, cols, rows, 8,
                                       cols * 4 * 8 / 8, colorSpace,
                                       bitmapInfo);//#4参数：每行数据的宽度。cols * 4(RGBA为一个unit) * 8(RGBA每个元素占8bit / 8(1byte = 8 bit))
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows),
                       image.CGImage);
    CGContextRelease(contextRef);
    MLMultiArray *imageArray = [[MLMultiArray alloc]initWithShape:@[@3, [NSNumber numberWithInteger:(int)cols], [NSNumber numberWithInteger:(int)rows]] dataType:MLMultiArrayDataTypeDouble error:nil];
    unsigned char *px = (unsigned char *)data;
    double r, g, b;
    for(int i = 0; i < rows; i++)
        for(int j = 0; j < cols; j++)
        {
            r = [self around:px[i * 96 * 4 + j * 4 + 0] / 255.0 decimals:12];
            g = [self around:px[i * 96 * 4 + j * 4 + 1] / 255.0 decimals:12];
            b = [self around:px[i * 96 * 4 + j * 4 + 2] / 255.0 decimals:12];
            //r = px[i * 96 * 4 + j * 4 + 0] / 255.0;
            //g = px[i * 96 * 4 + j * 4 + 1] / 255.0;
            //b = px[i * 96 * 4 + j * 4 + 2] / 255.0;
            [imageArray setObject:[NSNumber numberWithDouble:r] atIndexedSubscript:i * 96 + j];
            [imageArray setObject:[NSNumber numberWithDouble:g] atIndexedSubscript:1 * 96 * 96 + i * 96 + j];
            [imageArray setObject:[NSNumber numberWithDouble:b] atIndexedSubscript:2 * 96 * 96 + i * 96 + j];
        }
    return imageArray;
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
