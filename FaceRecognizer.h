//
//  FaceRecognizer.h
//  Face_Teacher
//
//  Created by jackstevenson on 2018/4/27.
//  Copyright © 2018年 jackstevenson. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ToolsForFaceRecognizer.h"
#import "FRmodel.h"
#import "OpencvTools.h"

@interface FaceRecognizer : NSObject
-(BOOL)isSamePerson:(MLMultiArray *)originalFace_vector and:(MLMultiArray *)sampleFace_vector;
-(NSArray *)imageToVectorOfNSArray:(UIImage *)faceImage;
-(MLMultiArray *)imageToVectorOfMLArray:(UIImage *)faceImage;
-(NSString *)vectorToJsonStr:(MLMultiArray *)faceVector;
-(NSString *)findFaceImage:(UIImage *)faceImage InDataSet:(NSDictionary *)dataSet;
@end
