//
//  Face_TeacherTests.m
//  Face_TeacherTests
//
//  Created by jackstevenson on 17/3/17.
//  Copyright © 2017年 jackstevenson. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <CoreData/CoreData.h>
#import <CoreImage/CoreImage.h>
#import <Foundation/Foundation.h>
#import "OpencvTools.h"
#import <stdio.h>
@interface Face_TeacherTests : XCTestCase

@end

//要进行面部检测相关的单元测试时需要将工程目录下对应的测试集添加到Xcode工程中后再进行测试，
//因为照片过多，添加到工程中后导致Xcode有点缓慢所以在平常的代码编写中测试集的照片是被从工程中移除了的，需要测试时再添加即可。
//测试集所需文件夹为
//orl_faces, FDDB-flods, imm_face_db, originalPics
//添加时按绝对路径添加
@implementation Face_TeacherTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

-(BOOL)isIntersect:(CGRect)rectA andRectB:(CGRect)rectB
{
    if(rectA.origin.x > rectB.origin.x)
    {
        CGRect temp = rectA;
        rectA = rectB;
        rectB = temp;
    }
    if(rectA.origin.x + rectA.size.width >= rectB.origin.x)
    {
        if(rectA.origin.y <= rectB.origin.y + rectB.size.height && rectB.origin.y + rectB.size.height <= rectA.origin.y + rectA.size.height) return YES;
        if(rectA.origin.y <= rectB.origin.y && rectA.origin.y + rectA.size.height >= rectB.origin.y) return YES;
        if(rectA.origin.y >= rectB.origin.y && rectA.origin.y + rectA.size.height <= rectB.origin.y + rectB.size.height) return YES;
    }
    return NO;
}
- (void)testRectIntersect
{
    //完全退化情况测试
    //1. 点点相交
    CGRect rect27 = CGRectMake(1, 1, 0, 0);
    XCTAssertTrue([self isIntersect:rect27 andRectB:rect27], "(1, 1, 0, 0) (1, 1, 0, 0)相交");
    //2. 点点不相交
    CGRect rect36 = CGRectMake(2, 2, 0, 0);
    XCTAssertFalse([self isIntersect:rect27 andRectB:rect36], "(1, 1, 0, 0) (2, 2, 0, 0)不相交");
    
    //非完全退化情况测试
    //3. 点 矩阵相交
    CGRect rect1 = CGRectMake(0, 0, 0, 0);
    CGRect rect2 = CGRectMake(0, 0, 10, 10);
    XCTAssertTrue([self isIntersect:rect1 andRectB:rect2], "(0,0,0,0) (0,0,10, 10)相交");
    XCTAssertTrue([self isIntersect:rect2 andRectB:rect1], "(0,0,10, 10) (0,0,0,0)相交");
    
    CGRect rect28 = CGRectMake(10, 0, 10, 10);
    CGRect rect29 = CGRectMake(0, 0, 10, 10);
    XCTAssertTrue([self isIntersect:rect28 andRectB:rect29], "(0,0,0,0) (0,0,10, 10)相交");
    XCTAssertTrue([self isIntersect:rect29 andRectB:rect28], "(0,0,10, 10) (0,0,0,0)相交");
    
    CGRect rect30 = CGRectMake(0, 10, 0, 0);
    CGRect rect31 = CGRectMake(0, 0, 10, 10);
    XCTAssertTrue([self isIntersect:rect30 andRectB:rect31], "(0, 10, 0, 0) (0, 0, 10, 10)相交");
    XCTAssertTrue([self isIntersect:rect31 andRectB:rect30], "(0, 0, 10, 10) (0, 10, 0, 0)相交");
    
    CGRect rect32 = CGRectMake(10, 10, 0, 0);
    CGRect rect33 = CGRectMake(0, 0, 10, 10);
    XCTAssertTrue([self isIntersect:rect32 andRectB:rect33], "(10,10,0,0) (0,0,10, 10)相交");
    XCTAssertTrue([self isIntersect:rect33 andRectB:rect32], "(0, 0, 10, 10) (10,10,0,0)相交");
    
    CGRect rect34 = CGRectMake(5, 5, 0, 0);
    CGRect rect35 = CGRectMake(0, 0, 10, 10);
    XCTAssertTrue([self isIntersect:rect34 andRectB:rect35], "(0,0,10,10)包含(5,5,0, 0)");
    XCTAssertTrue([self isIntersect:rect35 andRectB:rect34], "(0,0,10,10)包含(5,5,0, 0)");
    
    //4. 点 矩阵 不相交
    CGRect rect3 = CGRectMake(0, 0, 0, 0);
    CGRect rect4 = CGRectMake(10, 10, 10, 10);
    XCTAssertFalse([self isIntersect:rect3 andRectB:rect4], "(0,0,0,0) (10, 10, 10, 10)不相交");
    
    //正常情况
    //矩阵相交
    CGRect rect5 = CGRectMake(100, 100, 100, 100);
    CGRect rect6 = CGRectMake(10, 10, 100, 100);
    XCTAssertTrue([self isIntersect:rect5 andRectB:rect6], "(100, 100, 100, 100) (10, 10, 100, 100)相交");
    
    CGRect rect7 = CGRectMake(150, 150, 100, 100);
    CGRect rect8 = CGRectMake(100, 100, 100, 100);
    XCTAssertTrue([self isIntersect:rect7 andRectB:rect8], "(150,150,10,10) (100,100,100,100)相交");
    
    CGRect rect9 = CGRectMake(10, 10, 10, 10);
    CGRect rect10 = CGRectMake(15, 5, 10, 10);
    XCTAssertTrue([self isIntersect:rect9 andRectB:rect10], "(10,10,10,10) (15,5,10, 10)相交");
    
    CGRect rect11 = CGRectMake(10, 10, 10, 10);
    CGRect rect12 = CGRectMake(5, 5, 10, 10);
    XCTAssertTrue([self isIntersect:rect11 andRectB:rect12], "(10,10,10,10) (5,5,10,10)相交");
    
    CGRect rect13 = CGRectMake(10, 10, 10, 10);
    CGRect rect14 = CGRectMake(10, 15, 10, 10);
    XCTAssertTrue([self isIntersect:rect13 andRectB:rect14], "(10,10,10,10) (10,15,10, 10)算相交");
    
    CGRect rect15 = CGRectMake(10, 10, 10, 10);
    CGRect rect16 = CGRectMake(15, 15, 10, 10);
    XCTAssertTrue([self isIntersect:rect15 andRectB:rect16], "(10,10,10,10) (15,15,10, 10)算相交");
    CGRect rect17 = CGRectMake(15, 15, 2, 2);
    CGRect rect18 = CGRectMake(10, 10, 10, 10);
    XCTAssertTrue([self isIntersect:rect17 andRectB:rect18], "(15,15,2,2) (10,10,10,10)算相交");
    CGRect rect19 = CGRectMake(15, 16, 100, 100);
    CGRect rect20 = CGRectMake(10, 100, 100, 100);
    XCTAssertTrue([self isIntersect:rect19 andRectB:rect20], "(15,16,100,100) (10,100,100, 100)算相交");
    
    //矩阵不相交
    CGRect rect21 = CGRectMake(10, 10, 10, 10);
    CGRect rect22 = CGRectMake(1, 1, 1, 1);
    XCTAssertFalse([self isIntersect:rect21 andRectB:rect22], "(10,10,10,10) (1,1,1, 1)算相交");
    
    //共线情况
    CGRect rect23 = CGRectMake(309.00000, 40.00000, 52.0000, 52.0000);
    CGRect rect24 = CGRectMake(309.00000, 47.00000, 52.0000, 52.0000);
    XCTAssertTrue([self isIntersect:rect23 andRectB:rect24], "(309, 40, 52, 52) (309, 47, 52, 52)相交－－左边共线");
    XCTAssertTrue([self isIntersect:rect24 andRectB:rect23], "(309, 40, 52, 52) (309, 47, 52, 52)相交－－左边共线");
    
    CGRect rect25 = CGRectMake(253.000000, 59.000000, 62.000000, 62.000000);
    CGRect rect26 = CGRectMake(253.000000, 58.000000, 64.000000, 64.000000);
    XCTAssertTrue([self isIntersect:rect25 andRectB:rect26], "(253, 59, 62, 62) (253, 58, 64, 64)相交");
    
    //两边共线
    CGRect rect39 = CGRectMake(5, 5, 5, 5);
    CGRect rect40 = CGRectMake(0, 0, 10, 10);
    XCTAssertTrue([self isIntersect:rect39 andRectB:rect40], "(5, 5, 5, 5) (0, 0, 10, 10)相交");
    XCTAssertTrue([self isIntersect:rect40 andRectB:rect39], "(5, 5, 5, 5) (0, 0, 10, 10)相交");
    
    //三边共线
    CGRect rect41 = CGRectMake(1, 0, 9, 10);
    CGRect rect42 = CGRectMake(0, 0, 10, 10);
    XCTAssertTrue([self isIntersect:rect41 andRectB:rect42], "(1, 0, 9, 10) (0, 0, 10, 10)相交");
    XCTAssertTrue([self isIntersect:rect42 andRectB:rect41], "(0, 0, 10, 10) (1, 0, 9, 10)相交");
    
    //矩阵重合
    CGRect rect43 = CGRectMake(10, 10, 10, 10);
    XCTAssertTrue([self isIntersect:rect43 andRectB:rect43], "(10, 10, 10, 10) (10, 10, 10, 10)相交");
    
    //错误参数测试
    CGRect rect44 = CGRectMake(-10, -10, -100, -100);
    CGRect rect45 = CGRectMake(0, 0, 100, 100);
    XCTAssertFalse([self isIntersect:rect44 andRectB:rect45], "(-10, -10, -100, -100) (0, 0, 100, 100)不相交");
    
    CGRect rect46 = CGRectMake(-10, -10, -100, -100);
    CGRect rect47 = CGRectMake(100000, 100000, 100, 100);//超出视频区域
    XCTAssertFalse([self isIntersect:rect46 andRectB:rect47], "(-10, -10, -100, -100) (100000, 100000, 100, 100)不相交");
    
}

//从ciimage的坐标系变换到UIImage的坐标系
-(CGRect)changeCoordinate:(CGRect)rect forImage:(UIImage *)image
{
    CGRect newRect = CGRectMake(rect.origin.x,
                                image.size.height - rect.origin.y - rect.size.height,
                                rect.size.width,
                                rect.size.height);
    return newRect;
}


-(void)testIMMFaceDB
{
    int rightCount = 0;
    int count  = 0;
    NSDictionary * param = [NSDictionary dictionaryWithObject:CIDetectorAccuracyHigh forKey:CIDetectorAccuracy];
    CIDetector *faceDetector = [CIDetector detectorOfType:CIDetectorTypeFace context:nil options:param];
    NSString *rootPath= [[NSBundle mainBundle] resourcePath];
    const char *listFileStr = [[[rootPath stringByAppendingPathComponent:@"imm_face_db"] stringByAppendingPathComponent:@"path.txt"] UTF8String];
    FILE *listFile = fopen(listFileStr, "r");
    char imagePath_c[255];
    while(fscanf(listFile, "%s", imagePath_c) != EOF)
    {
        NSString *facePath = [NSString stringWithFormat:@"%@%@%@", rootPath, @"/", [NSString stringWithUTF8String:imagePath_c]];
        //NSLog(@"facePath:%@", facePath);
        UIImage *faceImage = [UIImage imageWithContentsOfFile:facePath];
        XCTAssertTrue(faceImage != nil);
        NSData *faceImageData = UIImagePNGRepresentation(faceImage);
        CIImage *ciimage = [CIImage imageWithData:faceImageData];
        NSArray *features = [faceDetector featuresInImage:ciimage];
        if([features count]) rightCount++;
        count++;
    }
    NSLog(@"right count:%d count:%d", rightCount, count);
    NSLog(@"rate : %lf", 1.0 * rightCount / count);

}

-(void)testFDDB
{
    int rightCount = 0;
    int count  = 0;
    NSDictionary * param = [NSDictionary dictionaryWithObject:CIDetectorAccuracyHigh forKey:CIDetectorAccuracy];
    CIDetector *faceDetector = [CIDetector detectorOfType:CIDetectorTypeFace context:nil options:param];
    NSString *rootPath= [[NSBundle mainBundle] resourcePath];
    const char *listFileStr = [[[rootPath stringByAppendingPathComponent:@"FDDB-folds"] stringByAppendingPathComponent:@"FDDB-fold-01-ellipseList.txt"] UTF8String];
    FILE *listFile = fopen(listFileStr, "r");
    char imagePath_c[255];
    double x, y;
    int n;
    while(fscanf(listFile, "%s", imagePath_c) != EOF)
    {
        NSMutableArray *facePoints = [[NSMutableArray alloc]init];
        double a, b, c, d;
        fscanf(listFile, "%d", &n);
        for(int i = 0; i < n; i++)
        {
            fscanf(listFile, "%lf %lf %lf %lf %lf %lf", &a, &b, &c, &x, &y, &d);
            CGPoint face_center = CGPointMake(x, y);
            [facePoints addObject:[NSValue valueWithCGPoint:face_center]];
            count++;
        }
        NSString *facePath = [NSString stringWithFormat:@"%@%@%@%@", rootPath, @"/originalPics/", [NSString stringWithUTF8String:imagePath_c], @".jpg"];
        //NSLog(@"facePath:%@", facePath);
        UIImage *faceImage = [UIImage imageWithContentsOfFile:facePath];
        XCTAssertTrue(faceImage != nil);
        NSData *faceImageData = UIImagePNGRepresentation(faceImage);
        CIImage *ciimage = [CIImage imageWithData:faceImageData];
        NSArray *features = [faceDetector featuresInImage:ciimage];
        for(CIFeature *f in features)
        {
            CGRect faceRect = [self changeCoordinate:[f bounds] forImage:faceImage];
            for(NSValue *v in facePoints)
            {
                if(CGRectContainsPoint(faceRect, [v CGPointValue]))
                {
                    rightCount++;
                    break;
                }
            }
        }
    }
    NSLog(@"FDDB rightCount:%d count:%d", rightCount, count);
    NSLog(@"rate : %lf", 1.0 * rightCount / count);
}

-(void)testPositiveSampleForFaceDetector
{
    int count  = 0;
    NSDictionary * param = [NSDictionary dictionaryWithObject:CIDetectorAccuracyHigh forKey:CIDetectorAccuracy];
    CIDetector *faceDetector = [CIDetector detectorOfType:CIDetectorTypeFace context:nil options:param];
    for(int i = 1; i <= 40; i++)
    {
        for(int j = 1; j <= 10; j++)
        {
            NSString *resPath= [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"orl_faces"];
            resPath = [resPath stringByAppendingPathComponent:[NSString stringWithFormat:@"s%d", i]];
            resPath = [resPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%d.pgm", j]];
            //NSLog(@"url=%@", resPath);
            
            UIImage *negativeImage = [UIImage imageWithContentsOfFile:resPath];
            NSData *data = UIImagePNGRepresentation(negativeImage);
            CIImage *negativeCIImage = [CIImage imageWithData:data];
            
            XCTAssertTrue(negativeCIImage != nil);
            NSArray *faces = [faceDetector featuresInImage:negativeCIImage];
            //XCTAssertTrue([faces count], "正样本有且仅有一张脸");
            if([faces count]) count++;
        }
    }
    NSLog(@"rate : %lf", 1.0 * count / 400);
}
-(void)testNegativeSampleForFaceDetector
{
    NSDictionary * param = [NSDictionary dictionaryWithObject:CIDetectorAccuracyHigh forKey:CIDetectorAccuracy];
    CIDetector *faceDetector = [CIDetector detectorOfType:CIDetectorTypeFace context:nil options:param];
    for(int i = 1; i <= 20; i++)
    {
        UIImage *negativeImage = [UIImage imageNamed:[NSString stringWithFormat:@"classroom%d", i]];
        NSArray *faces = [faceDetector featuresInImage:negativeImage.CIImage];
        XCTAssertFalse([faces count], "负样本无face");
    }
}


@end
